<div id="tab-photos">
    <ul id="sortable-photos" class="list-photos">
        @if(!empty($project))
        @foreach($project->medias as $rs)
            @component('admin.layouts.components.li_photo', [
                'photo_id' => $rs->id,
                'photo_path' => $rs->path,
                'input_delete' => 'delete_photos[]'
            ])
            @endcomponent
        @endforeach
        @endif
    </ul>

    <div class="text-center">
        <button type="button" class="btn btn-primary ckfinder-multi" data-append="#sortable-photos"
                data-name="photos[]">
            {{ trans('button.add_photos') }}
        </button>
    </div>
</div>

<div class="clear-fix"></div>

<div class="row" style="margin-top: 10px">
    <div class="col-md-4">
        <div class="font-bold col-green">{!! trans('admin_project.form.position') !!}</div>
        <div class="form-group form-float">
            <div class="form-line">
                <input type="number" class="form-control" name="position" value="{{ $project->position ?? 0 }}" required
                       min="0">
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="font-bold col-green">{!! trans("admin_project.form.category") !!}</div>
        <div class="form-group form-float">
            <select name="project_category_id" id="project_category_id" class="form-control">
                <option value="">---</option>
                @if(!empty($project_category))
                @foreach($project_category as $key => $value)
                    <option value="{{ $value->id }}" {{ !empty($project) && $project->project_category_id == $value->id ? 'selected' : '' }}>{{ $value->name }}</option>
                @endforeach
                @endif
            </select>
        </div>
    </div>
</div>

@include('admin.translation.nav_tab', [
    'object_trans' => $project ?? null,
    'default_tab' => $composer_locale,
    'form_fields' => [
        ['type' => 'text', 'name' => 'name'],
        ['type' => 'textarea', 'name' => 'description'],
        ['type' => 'ckeditor', 'name' => 'content']
    ],
    'form_plugins' => ['ckeditor'],
    'metadata' => $metadata ?? null,
    'translation_file' => 'admin_project',
])