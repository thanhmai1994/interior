@extends("admin.layouts.master")

@section("meta")

@endsection

@section("style")

@endsection

@section("content")
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="body">

                @include("admin.layouts.partials.message")

                @component('admin.layouts.components.form', [
                'form_method' =>  empty($project_category) ? 'POST' : 'PUT',
                'form_url' => empty($project_category) ? route("admin.project.category.store") : route("admin.project.category.update", $project_category->id)
                ])

                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group form-float">
                                <div class="font-bold col-green">{{ trans('admin_project_category.form.type') }}</div>
                                <div class="form-line focused">
                                    <select id="type" class="form-control" name="type">
                                        @foreach($types as $key => $value)
                                            <option {{ (!empty($project_category) && ($project_category->type == $key)) ? "selected" : "" }} value="{{ $key }}">{{ $value }}</option>
                                        @endforeach
                                    
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>

                    @include('admin.translation.nav_tab', [
                        'object_trans' => $project_category ?? null,
                        'default_tab' => $composer_locale,
                        'form_fields' => [
                            ['type' => 'photo_translation', 'name' => 'photo_translation'],
                            ['type' => 'text', 'name' => 'name'],
                            ['type' => 'textarea', 'name' => 'description'],
                        ],
                        'translation_file' => 'admin_project_category'
                    ])

                    @include("admin.layouts.partials.form_buttons", [
                        "cancel" => route("admin.project.category.index")
                    ])
                @endcomponent

                </div>
            </div>
        </div>
    </div>

    
@endsection

@section("script")
    <!-- Jquery Validation Plugin Css -->
    <script src="/assets/plugins/jquery-validation/jquery.validate.js"></script>

    @if($composer_locale !== 'en')
        <script type="text/javascript"
                src="/assets/plugins/jquery-validation/localization/messages_{{ $composer_locale }}.js"></script>
    @endif

    <script type="text/javascript" src="/assets/admin/js/pages/project_category.create.js?v=1.0"></script>

    @php
        $single_menu =  json_encode(config('constants.single_menu'), JSON_UNESCAPED_SLASHES);
    @endphp
@endsection