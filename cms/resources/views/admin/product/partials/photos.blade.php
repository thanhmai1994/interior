<ul id="sortable-photos" class="list-photos">
    
    @isset($product)
        @foreach($product->medias as $rs)
            @component('admin.layouts.components.li_photo', [
                'photo_id' => $rs->id,
                'photo_path' => $rs->path,
                'input_delete' => 'delete_photos[]'
            ])
            @endcomponent
        @endforeach
    @endisset
        
</ul>
<div class="row" style="margin-top: 10px">
    <div class="col-md-6 col-sm-6">
        <div class="font-bold col-green">{!! trans("admin_product.form.position") !!}</div>
        <div class="form-group form-float">
            <div class="form-line">
                <input type="number" class="form-control" name="position" value="{{ $product->position ?? 0 }}" required min="0">
            </div>
        </div>
    </div>
</div>
<div class="text-center">
    <button type="button" class="btn btn-primary ckfinder-multi" data-append="#sortable-photos" data-name="photos[]">
        {{ trans('button.add_photos') }}
    </button>
</div>

@include('admin.translation.nav_tab', [
    'object_trans' => $product ?? null,
    'default_tab' => $composer_locale,
    'form_fields' => [
        ['type' => 'text', 'name' => 'name'],
        ['type' => 'textarea', 'name' => 'description']
        ['type' => 'ckeditor', 'name' => 'content']
    ],
    'form_plugins' => ['ckeditor'],
    'metadata' => $metadata ?? null,
    'translation_file' => 'admin_product',
])