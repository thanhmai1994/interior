<div id="tab-photos">
    <ul id="sortable-photos" class="list-photos">
        @if(!empty($product))
        @foreach($product->medias as $rs)
            @component('admin.layouts.components.li_photo', [
                'photo_id' => $rs->id,
                'photo_path' => $rs->path,
                'input_delete' => 'delete_photos[]'
            ])
            @endcomponent
        @endforeach
        @endif
    </ul>

    <div class="text-center">
        <button type="button" class="btn btn-primary ckfinder-multi" data-append="#sortable-photos"
                data-name="photos[]">
            {{ trans('button.add_photos') }}
        </button>
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <input type="checkbox" id="active" name="active"
                   value="1" {!! !empty($product) && $product->active ? "checked" : null !!}>
            <label for="active">{!! trans("admin_product.form.active") !!}</label>
        </div>
    </div>
</div>

<div class="clear-fix"></div>

@include('admin.translation.nav_tab', [
    'object_trans' => $product ?? null,
    'default_tab' => $composer_locale,
    'form_fields' => [
        ['type' => 'text', 'name' => 'name'],
        ['type' => 'textarea', 'name' => 'description'],
        ['type' => 'ckeditor', 'name' => 'content']
    ],
    'form_plugins' => ['ckeditor'],
    'metadata' => $metadata ?? null,
    'translation_file' => 'admin_product',
])