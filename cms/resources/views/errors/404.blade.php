@extends('frontend.layouts.master')

@section('content')

<div class="page-banner">
    <img src="img/404.png" alt="Page not found">
    <div class="container">
        <div class="inner-banner">
            <div class="tbl-cell"></div>
        </div>
    </div>
</div>
<div class="notFound"></div>

@endsection