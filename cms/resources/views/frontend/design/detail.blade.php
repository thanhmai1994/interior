@extends('frontend.layouts.master')

@section('content')

<div class="page-banner">
    <img src="{{ count($design->medias) && !empty($design->medias->sortBy('position')[0]) ? getThumbnail($design->medias->sortBy('position')[0]->path, 1400, 467) : '' }}" alt="">
    <div class="container">
        <div class="inner-banner">
            <div class="tbl-cell">
                <h4>{{ $design->name }}</h4>
            </div>
        </div>
    </div>
</div>
<div class="mainpage">
    <div class="container">
        
        @include('frontend.layouts.partials.breadcrumb')

        <div class="maincontent">
            <div class="row">
                <div class="col-md-3">
                    <div class="wrap-menuchild"></div>
                    <h4 class="t-header-4">Danh mục</h4><a class="btn-nav" href="#">Navigation</a>
                    <div class="mb-tab">
                        <ul class="menuchild">
                            @if(!empty($design_category_all))
                            @foreach($design_category_all as $key)
                            <li class="{{ !empty(Request::segment(2)) && $key->slug == Request::segment(2) ? 'active' : '' }}"><a href="{{ route('design.show', $key->slug) }}">{{ $key->name }}</a></li>
                            @endforeach
                            @endif
                        </ul>
                    </div>
                    {{-- <div class="avd-left"><img src="/assets/images/tempt/img10.jpg" alt="">
                        <div class="info-adv">
                            <h4>Contact Us</h4><a class="v-more effect-button" data-hover="CLICK HERE" tabindex="0" href="#"><span>CLICK HERE</span></a>
                        </div>
                    </div> --}}
                </div>
                <div class="col-md-9">
                    <h4 class="t-header-2">{{ $design->name }}</h4>
                    <h4 class="t-header-3">{!! $design->description !!}</h4>
                    <div class="silder-prj">
                        <div class="slider-pr">
                            @if(!empty($design) && count($design->medias))
                            @foreach($design->medias as $key)
                            <div><img src="{{ getThumbnail($key->path, 875, 446) }}" alt="{{ $design->name }}"></div>
                            @endforeach
                            @endif
                        </div>
                    </div>
                    <div class="box-3">
                        {!! $design->content !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection