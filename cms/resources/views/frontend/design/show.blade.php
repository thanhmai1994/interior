@extends('frontend.layouts.master')

@section('content')

<div class="page-banner">
    <img src="{{ getThumbnail($design_category->photo_translation, 1400, 467) }}" alt="">
    <div class="container">
        <div class="inner-banner">
            <div class="tbl-cell">
                <h4>{{ $design_category->name }}</h4>
            </div>
        </div>
    </div>
</div>
<div class="mainpage">
    <div class="container">
        
        @include('frontend.layouts.partials.breadcrumb')

        <div class="maincontent">
            <div class="row">
                <div class="col-md-3">
                    <div class="wrap-menuchild"></div>
                    <h4 class="t-header-4">Danh mục</h4>
                    <a class="btn-nav" href="#">Navigation</a>
                    <div class="mb-tab">
                        <ul class="menuchild">
                            @if(!empty($design_category_all))
                            @foreach($design_category_all as $key)
                            <li class="{{ $key->slug == $design_category->slug ? 'active' : '' }}"><a href="{{ route('design.show', $key->slug) }}">{{ $key->name }}</a></li>
                            @endforeach
                            @endif
                        </ul>
                    </div>
                    {{-- <div class="avd-left"><img src="/assets/images/tempt/img10.jpg" alt="">
                        <div class="info-adv">
                            <h4>Contact Us</h4><a class="v-more effect-button" data-hover="CLICK HERE" tabindex="0" href="#"><span>CLICK HERE</span></a>
                        </div>
                    </div> --}}
                </div>
                <div class="col-md-9">
                    <h4 class="t-header-2">{{ $design_category->name }}</h4>
                    <h4 class="t-header-3">{!! $design_category->description !!}</h4>
                    <div class="grid-6">
                        @if($design_category->designs->count())
                        @foreach($design_category->designs as $key)
                        <div class="grid-6-item">
                            <div class="grid-6-img">
                                <a class="over-link" 
                                    href="{{ route('design.detail', [
                                        'slug_category' => $design_category->slug,
                                        'slug' => $key->slug
                                    ]) }}"></a>

                                <img src="{{ count($key->medias) && !empty($key->medias->sortBy('position')[0]) ? getThumbnail($key->medias->sortBy('position')[0]->path, 269, 247) : '' }}">
                                <div class="grid-6-hide"></div>
                            </div>
                            <h4>{{ $key->name }}</h4>
                        </div>
                        @endforeach
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection