@extends('frontend.layouts.master')

@section('content')

<div class="page-banner">
    <img src="{{ count($project->medias) && !empty($project->medias->sortBy('position')[0]) ? getThumbnail($project->medias->sortBy('position')[0]->path, 1400, 467) : '' }}" alt="">
    <div class="container">
        <div class="inner-banner">
            <div class="tbl-cell">
                <h4>{{ $project->name }}</h4>
            </div>
        </div>
    </div>
</div>
<div class="mainpage">
    <div class="container">
        
        @include('frontend.layouts.partials.breadcrumb')

        <div class="maincontent">
            <div class="row">
                <div class="col-md-3">
                    <div class="wrap-menuchild"></div>
                    <h4 class="t-header-4">Danh mục</h4><a class="btn-nav" href="#">Navigation</a>
                    <div class="mb-tab">
                        <ul class="menuchild">
                            @if(!empty($project_category_all))
                            @foreach($project_category_all as $key)
                            <li class="{{ !empty(Request::segment(2)) && $key->slug == Request::segment(2) ? 'active' : '' }}"><a href="{{ route('project.show', $key->slug) }}">{{ $key->name }}</a></li>
                            @endforeach
                            @endif
                        </ul>
                    </div>
                    {{-- <div class="avd-left"><img src="/assets/images/tempt/img10.jpg" alt="">
                        <div class="info-adv">
                            <h4>Contact Us</h4><a class="v-more effect-button" data-hover="CLICK HERE" tabindex="0" href="#"><span>CLICK HERE</span></a>
                        </div>
                    </div> --}}
                </div>
                <div class="col-md-9">
                    <h4 class="t-header-2">{{ $project->name }}</h4>
                    <h4 class="t-header-3">{!! $project->description !!}</h4>
                    <div class="silder-prj">
                        <div class="slider-pr">
                            @if(!empty($project) && count($project->medias))
                            @foreach($project->medias as $key)
                            <div><img src="{{ getThumbnail($key->path, 875, 446) }}" alt="{{ $project->name }}"></div>
                            @endforeach
                            @endif
                        </div>
                    </div>
                    <div class="box-3">
                        {!! $project->content !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection