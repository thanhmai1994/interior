<!DOCTYPE html>
<html lang="{{ $composer_locale }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <base href="{{asset('/')}}">

    @section("seo")
        @include('frontend.layouts.partials.seo')
    @show

    <!--The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags-->
    <meta name="description" content="">
    <meta name="author" content="">
    <link href="apple-touch-icon.png" rel="apple-touch-icon">
    <link href="favicon.ico" rel="icon">
    <title> Fifth Avenue Interior Pte Ltd</title>
    <link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,400i,700,700i,900,900i&amp;subset=vietnamese" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,400i,500,500i,700,700i,900,900i&amp;subset=vietnamese" rel="stylesheet">

    <link href="/assets/css/plugin.css" rel="stylesheet">
    <link href="/assets/css/main.css" rel="stylesheet">

    @yield('style')

</head>
<body>

    <div class="page">

        @include('frontend.layouts.partials.header')

        @yield('content')

    </div>

    @include('frontend.layouts.partials.footer')

    <script type="text/javascript" src="/assets/js/jquery.min.js"></script>
    <script type="text/javascript" src="/assets/js/plugin.js"></script>
    <script type="text/javascript" src="/assets/js/main.js"></script>

    @include('frontend.layouts.partials.alert_modal')

    @stack("add_script")

    @yield('script')

    {!! System::content('chat_script', null) !!}

</body>
</html>