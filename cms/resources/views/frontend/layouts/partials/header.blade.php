<header class="header">
    <div class="container">
        <div class="logo">
            <a href="/"><img src="{{ System::content('website_logo','#') }}" alt=""></a>
        </div>
        <div class="wrapmenu">
            <a class="menu-page btn-menu" href="#menu">
                <span class="burger-icon-1"></span>
                <span class="burger-icon-2"></span>
                <span class="burger-icon-3"></span>
            </a>
            <div class="mainmenu" id="menu">
                {!! getHeaderHtml() !!}
                {{-- <ul class="menu">
                    <li class="active"><a href="index.html"><span><i class="fa fa-home"></i></span>Trang Chủ</a></li>
                    <li><a href="gioithieu.html"><span><i class="fa fa-pencil"></i></span>Giới thiệu</a></li>
                    <li><a href="quytrinh.html"><span><i class="fa fa-cogs"></i></span>Quy Trình </a></li>
                    <li><a href="duan.html"><span><i class="fa fa-wrench"></i></span>Dự án</a></li>
                    <li><a href="duan.html"><span><i class="fa fa-play-circle"></i></span> Thiết kế nội thất</a></li>
                    <li><a href="#"><span><i class="fa fa-bullhorn"></i></span> Sản phẩm</a></li>
                    <li><a href="lienhe.html"><span><i class="fa fa-paper-plane"></i></span>Liên hệ</a></li>
                </ul> --}}
            </div>
        </div>
    </div>
</header>