<footer class="footer">
    <div class="container">
        <div class="col-f2">
            {!! getFooterHtml() !!}
        </div>
        <div class="col-f3 share">
            <p>Get In Touch With Us</p>
            <p>
                <a href="{{ System::content('facebook','#') }}"><i class="fa fa-facebook" aria-hidden="true"></i>
                </a><a href="{{ System::content('twitter','#') }}"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                <a href="{{ System::content('google','#') }}"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
            </p>
        </div>
    </div>
</footer>