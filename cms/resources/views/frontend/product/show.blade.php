@extends('frontend.layouts.master')

@section('content')

<div class="page-banner">
    <img src="{{ count($product->medias) && !empty($product->medias->sortBy('position')[0]) ? getThumbnail($product->medias->sortBy('position')[0]->path, 1400, 467) : '' }}" alt="">
    <div class="container">
        <div class="inner-banner">
            <div class="tbl-cell">
                <h4>{{ $product->name }}</h4>
            </div>
        </div>
    </div>
</div>
<div class="mainpage">
    <div class="container">
        
        @include('frontend.layouts.partials.breadcrumb')
        
        <div class="maincontent">
            <div class="row">
                <div class="col-md-6">
                    <div class="silder-prj">
                        <div class="slider-pr">
                            @if(!empty($product) && count($product->medias))
                            @foreach($product->medias->sortBy('position') as $key)
                            <div><img src="{{ getThumbnail($key->path, 875, 446) }}" alt=""></div>
                            @endforeach
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <h4 class="t-header-2">{{ $product->name }}</h4>
                    <div class="box-3 proDetail">
                        <h5>Mô tả: </h5>
                        {!! $product->description !!}
                        
                        {!! $product->content !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection