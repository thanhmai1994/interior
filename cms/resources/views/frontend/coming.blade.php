@extends('frontend.layouts.master')

@section('content')

<div class="page-banner">
    <img src="/assets/images/tempt/slide-1.jpg" alt="Page not found">
    <div class="container">
        <div class="inner-banner">
            <div class="tbl-cell">
                <h4>Coming soon</h4>
            </div>
        </div>
    </div>
</div>
<div class="notFound"></div>

@endsection