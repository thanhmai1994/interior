@extends('frontend.layouts.master')

@section('content')

<div class="page-banner">
    <img src="{!! $blocks['PRODUCT-SLIDER'][0]->photo !!}" alt="">
    <div class="container">
        <div class="inner-banner">
            <div class="tbl-cell">
                <h4>{!! $blocks['PRODUCT-SLIDER'][0]->name !!}</h4>
            </div>
        </div>
    </div>
</div>
<div class="mainpage">
    <div class="container">
        
        @include('frontend.layouts.partials.breadcrumb')
        
        <div class="maincontent">
            <div class="row">
                <div class="col-md-12">
                    <h4 class="t-header-2">{!! $blocks['PRODUCT-GENERAL'][0]->name !!}</h4>
                    <h4 class="t-header-3">{!! $blocks['PRODUCT-GENERAL'][0]->content !!}</h4>
                    <div class="grid-6">
                        @if(!empty($product))
                        @foreach($product as $key)
                        <div class="grid-6-item">
                            <div class="grid-6-img">
                                <a class="over-link" href="{{ route('product.show', $key->slug) }}"></a><img src="{{ count($key->medias) && !empty($key->medias->sortBy('position')[0]) ? getThumbnail($key->medias->sortBy('position')[0]->path, 930, 530) : '' }}">
                                <div class="grid-6-hide"></div>
                            </div>
                            <h4>{{ $key->name }}</h4>
                        </div>
                        @endforeach
                        @endif
                    </div>
                    {{ $product->links('vendor.pagination.bootstrap-4') }}
                </div>
            </div>
        </div>
    </div>
</div>

@endsection