@extends('frontend.layouts.master')

@section('content')

<div class="page-banner">
    <img src="{!! $blocks['PROCESS-SLIDER'][0]->photo !!}" alt="">
    <div class="container">
        <div class="inner-banner">
            <div class="tbl-cell">
                <h4>{!! $blocks['PROCESS-SLIDER'][0]->name !!}</h4>
            </div>
        </div>
    </div>
</div>
<div class="mainpage">
    <div class="container">
        
        @include('frontend.layouts.partials.breadcrumb')

        <div class="maincontent">
            <h4 class="t-header-2">{!! $blocks['PROCESS-GENERAL'][0]->name !!}</h4>
            <div class="grid-2">
                @if(!empty($blocks['PROCESS-GENERAL'][0]->children)) 
                @foreach($blocks['PROCESS-GENERAL'][0]->children as $key => $block)
                <div class="grid-2-item">
                    <div class="grid-2-img">
                        <img src="{{ $block->icon }}" alt>
                    </div>
                    <div class="grid-2-info">
                        <h4>{!! $block->name !!}</h4>
                        <!-- <p>An <strong>in-depth analysis</strong> of the client’s brief and requirements will be done
                            for a short period of time. We will then give you back a <strong>FREE estimated quotation
                                for you to review</strong>.</p> -->
                        {!! $block->content !!}
                    </div>
                </div>
                @endforeach
                @endif
                {{-- <div class="grid-2-item">
                    <div class="grid-2-img"><img src="/assets/images/tempt/icon-2.png" alt></div>
                    <div class="grid-2-info">
                        <h4>Contruction</h4>
                        <p>After design and concept has been finalized, we begin with the real work; <strong>this is
                                where your vision turns into reality</strong>. We will work closely with the contractor
                            and suppliers to ensure everything has been
                            installed properly and functions correctly, with no malfunctions or errors. </p>
                        <p>We will always keep you updated with the status of this project – your dream home.</p>
                    </div>
                </div>
                <div class="grid-2-item">
                    <div class="grid-2-img"><img src="/assets/images/tempt/icon-3.png" alt></div>
                    <div class="grid-2-info">
                        <h4>Space Planning</h4>
                        <p>A fundamental part of the interior design process, Space Planning will first start off with
                            a <strong>site visit and survey of facilities</strong>. This is to <strong>measure up the
                                space and envision the movements</strong> that will happen in those areas. Together,<strong>
                                we will re-organize</strong> a lay-out to determine cost-efficient and space-saving
                            solutions.</p>
                    </div>
                </div>
                <div class="grid-2-item">
                    <div class="grid-2-img"><img src="/assets/images/tempt/icon-4.png" alt></div>
                    <div class="grid-2-info">
                        <h4>Project Turnover</h4>
                        <p><strong>The moment of truth, the big reveal!</strong> After wrapping up with some additional
                            touch-ups, cleaning, and moving in your furniture, we <strong>turn over the finished
                                product</strong> to you.</p>
                    </div>
                </div>
                <div class="grid-2-item">
                    <div class="grid-2-img"><img src="/assets/images/tempt/icon-5.png" alt></div>
                    <div class="grid-2-info">
                        <h4>Conceptualization of Design</h4>
                        <p>Conceptualization will then lead to a concrete design idea. This is where we <strong>brainstorm
                                for creative concepts and practical, cost-saving solutions</strong>, to be proposed in
                            our lay-out plan. We can plan the color
                            theme, lights, fabrics, furniture, and so on.</p>
                    </div>
                </div>
                <div class="grid-2-item">
                    <div class="grid-2-img"><img src="/assets/images/tempt/icon-6.png" alt></div>
                    <div class="grid-2-info">
                        <h4>After-Sales Service</h4>
                        <p>Even after the completion of project, you can still contact us if <a href="#">there’s any
                                defects</a> occur. We take full responsibility and will do what we can to resolve these
                            issues, and we also <strong>cover warranty</strong> to assure you quality all the way.</p>
                        <p>Every now and then, <strong>we’ll check with you how’s everything going and we also do
                                site-visits</strong> to make sure everything is in place.</p>
                    </div>
                </div> --}}
            </div>
        </div>
    </div>
</div>

@endsection