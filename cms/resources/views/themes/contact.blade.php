@extends('frontend.layouts.master')

@section('content')

<div class="page-banner">
    <img src="{!! $blocks['CONTACT-SLIDER'][0]->photo !!}" alt="">
    <div class="container">
        <div class="inner-banner">
            <div class="tbl-cell">
                <h4>{!! $blocks['CONTACT-SLIDER'][0]->name !!}</h4>
            </div>
        </div>
    </div>
</div>
<div class="mainpage">
    <div class="container">
        
        @include('frontend.layouts.partials.breadcrumb')

        <div class="maincontent">
            <div class="widget-map">
                {{-- <div class="map_canvas" id="map_canvas" style="height:240px;"></div> --}}
                {!! $blocks['CONTACT-MAP'][0]->description !!}
            </div>
            <div class="grp-contact-page clearfix">
                <div class="cnt-left">
                    <div class="box-address">   
                        <div class="img-adress"><img src="/assets/images/tempt/cnt-img.jpg" alt=""></div>
                        <div class="info-adress">
                            <h4>{!! $blocks['CONTACT-GENERAL'][0]->name !!}</h4>
                            <table>
                                @if(!empty($blocks['CONTACT-GENERAL'][0]->content))
                                    {!! $blocks['CONTACT-GENERAL'][0]->content !!}
                                @else
                                    <tr>
                                        <td class="icon-cnt"><span><i class="fa fa-map-marker"></i></span></td>
                                        <td colspan="3">81 Ubi Avenue 4 #01-11 UB.One Building Singapore 408830</td>
                                    </tr>
                                    <tr>
                                        <td class="icon-cnt"><span><i class="fa fa-phone"></i></span></td>
                                        <td>+65 6443 9530 </td>
                                        <td class="icon-cnt"><span><i class="fa fa-fax"></i></span></td>
                                        <td>+65 6443 9530 </td>
                                    </tr>
                                    <tr>
                                        <td class="icon-cnt"><span><i class="fa fa-envelope"></i></span></td>
                                        <td colspan="3"><a href="mailto:enquiries@fifthavenue.com.sg">enquiries@fifthavenue.com.sg</a></td>
                                    </tr>
                                @endif
                                
                            </table><a class="v-more effect-button" data-hover="GET DIRECTIONS" tabindex="0" href="{!! $blocks['CONTACT-GENERAL'][0]->url !!}"><span>GET
                                    DIRECTIONS</span></a>
                        </div>
                    </div>

                </div>
                <div class="cnt-right">
                    <div class="form-cnt">
                        {{-- <p>We welcome your comments and feedback, feel free to get in touch with us using the form
                            below. We will get a designer to attend to you at the soonest.<br />We look forward to
                            hearing from you!</p> --}}
                        
                        {!! $blocks['CONTACT-FORM'][0]->description !!}
                        
                        <div class="form-style">

                            @include('frontend.layouts.partials.alert')

                            <form action="{{ route('page.storeContact') }}" method="POST">

                                {!! csrf_field() !!}

                                <div class="form-item">
                                    <label class="lb1">Name <span class="sys">:</span></label>
                                    <div class="form-ipt">
                                        <input class="form-control" type="text" name="name">
                                    </div>
                                </div>
                                <div class="form-item">
                                    <label class="lb1">Email <span class="sys">:</span></label>
                                    <div class="form-ipt">
                                        <input class="form-control" type="text" name="email">
                                    </div>
                                </div>
                                <div class="form-item">
                                    <label class="lb1">Address <span class="sys">:</span></label>
                                    <div class="form-ipt">
                                        <input class="form-control" type="text" name="address">
                                    </div>
                                </div>
                                <div class="form-item">
                                    <label class="lb1">Comments/Feedback <span class="sys">:</span></label>
                                    <div class="form-ipt">
                                        <textarea class="form-control" name="content"></textarea>
                                    </div>
                                </div>
                                <div class="form-item">
                                    <label class="lb1"> </label>
                                    <div class="form-ipt">
                                        <div class="grp-btn">
                                            <button class="btn btn-sm" type="submit">Submit</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?key={{ config('services.google.map_key') }}"></script>
<script type="text/javascript" src="/assets/js/map.js"></script>

@endsection