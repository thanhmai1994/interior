@extends('frontend.layouts.master')

@section('content')

<div class="page_mainslider">
    <div id="slider">
        @if(!empty($slider))
        @foreach($slider as $key)
        <div class="bgslider">
            <a class="link-over" href="{{ $key->link }}"></a><img class="bgimg" src="{{ $key->image }}" alt="">
            <div class="container">
                <div class="infobanner">
                    <div class="tbl-cell">
                        <div class="des-info">
                            <h4>{!! $key->name !!}</h4>
                            <p>{!! $key->description !!}</p><a class="link-slider" href="{{ $key->link }}">READ MORE</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
        @endif
    </div>
</div>
<div class="mainhome">
    <div class="box1-img"><img src="{!! $blocks['HOME-GENERAL'][0]->photo !!}" alt=""></div>
    <div class="container">
        <div class="box1">
            <div class="box1-left">
                <div class="t-header-1">
                    <!-- <p>Welcome to</p>
                    <h1>FIFTH AVENUE INTERIOR</h1> -->
                    {!! $blocks['HOME-GENERAL'][0]->name !!}
                </div>
                <div class="box1-info">
                    <p>{!! $blocks['HOME-GENERAL'][0]->description !!}</p>
                    <a class="v-more effect-button" data-hover="Xem chi tiết" tabindex="0" href="{!! $blocks['HOME-GENERAL'][0]->url !!}">
                        <span>Xem chi tiết</span>
                    </a>
                </div>
            </div>
        </div>
        <div class="box2">
            <img src="{!! $blocks['HOME-IMAGE-CENTER'][0]->photo !!}" alt="">
        </div>
        <div class="grid1">
            @if(!empty($blocks['HOME-MENU'][0]->children)) 
            @foreach($blocks['HOME-MENU'][0]->children as $key => $block)
            <div class="grid1-item">
                <div class="grid1-img">
                    <a class="over-link" href="{{ $block->url }}"></a>
                    <img src="{{ $block->photo }}" alt=""><span class="hover"></span>
                </div>
                <div class="grid1-info">
                    <h4><span>{!! $block->name !!}</span></h4>
                    <p>{!! $block->description !!}</p>
                    <a class="r-more" href="{{ $block->url }}">Xem chi tiết</a>
                </div>
            </div>
            @endforeach
            @endif
        </div>
    </div>
</div>

@endsection