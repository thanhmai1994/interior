@if($blocks && $blocks->has('BANNER') && $bt = $blocks->get('BANNER')->first())
    <section class="breadcrumbs breadcrumbs--blue" style="background-image: url({{$bt->photo}})" data-paroller-factor="0.4" data-paroller-factor-xs="0.2" data-paroller-factor-sm="0.3">
        <div class="container text-center">
            <h1>{{$bt->name}}</h1>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{route('page.home')}}" title="Trang chủ">Trang chủ</a></li>
                    @foreach($bt->children as $index=>$child)
                        <li class="breadcrumb-item"><a href="{{!$index ? $child->url : 'javascript:void(0)'}}" title="{{$child->name}}">{{$child->name}}</a></li>
                    @endforeach
                </ol>
            </nav>
        </div>
    </section>
@endif