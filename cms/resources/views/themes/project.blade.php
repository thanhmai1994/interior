@extends('frontend.layouts.master')

@section('content')

<div class="page-banner">
    <img src="{!! $blocks['PROJECT-SLIDER'][0]->photo !!}" alt="">
    <div class="container">
        <div class="inner-banner">
            <div class="tbl-cell">
                <h4>{!! $blocks['PROJECT-SLIDER'][0]->name !!}</h4>
            </div>
        </div>
    </div>
</div>
<div class="mainpage">
    <div class="container">
        
        @include('frontend.layouts.partials.breadcrumb')
        
        <div class="maincontent">
            <h4 class="t-header-2">{!! $blocks['PROJECT-GENERAL'][0]->name !!}</h4>
            <h4 class="t-header-3">{!! $blocks['PROJECT-GENERAL'][0]->content !!}</h4>
            <div class="grid-5">
                @if(!empty($project_category))
                @foreach($project_category as $key)
                <div class="grid-5-item">
                    <div class="grid-5-img">
                        <a class="over-link" href="{{ route('project.show', $key->slug) }}"></a>
                        <img src="{{ $key->photo_translation }}" alt>
                        <div class="grid-5-info">
                            <div class="tbl-full">
                                <div class="tbl-cell">
                                    <h4>{{ $key->name }}</h4>
                                </div>
                            </div>
                        </div>
                        <div class="grid-5-des-hide">
                            <div class="tbl-full">
                                <div class="tbl-cell">
                                    <h4>{{ $key->name }}</h4>
                                    <a class="r-more" href="{{ route('project.show', $key->slug) }}">Xem chi tiết</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
                @endif
            </div>
        </div>
    </div>
</div>

@endsection