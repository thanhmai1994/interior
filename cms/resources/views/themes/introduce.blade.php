@extends('frontend.layouts.master')

@section('content')

<div class="page-banner">
    <img src="{!! $blocks['INTRODUCE-SLIDER'][0]->photo !!}" alt="">
    <div class="container">
        <div class="inner-banner">
            <div class="tbl-cell">
                <h4 style="color:#fff;">{!! $blocks['INTRODUCE-SLIDER'][0]->name !!}</h4>
            </div>
        </div>
    </div>
</div>
<div class="mainpage">
    <div class="container">

        @include('frontend.layouts.partials.breadcrumb')

        <div class="maincontent">
            <div class="row">
                <div class="col-md-12">
                    <!-- <h4 class="t-header-2">Our Story</h4>
                    <h5 class="t-header-3">Your Dream, We Actualize.</h5> -->
                    {!! $blocks['INTRODUCE-GENERAL'][0]->name !!}

                    <div class="img-ab">
                        <img class="imgright" src="{!! $blocks['INTRODUCE-GENERAL'][0]->photo !!}" alt="">
                    </div>
                    <div class="document">
                        <!-- <p>Fifth Avenue Interior is a local-based interior design firm in Singapore that offers <strong>end-to-end
                                interior design services for commercial, residential and office spaces.</strong></p>
                        <p>Composed of a group of friends who later decided to join forces in creating their own
                            interior design agency, we are young, creative, and well-experienced designers whose main
                            goal is to redefine spaces and turn them into
                            art. </p>
                        <p>The name ‘avenue’ is also another term for solution, which is what Fifth Avenue does for
                            their clients – <strong>providing renovation and artistic solutions in creating everyone’s
                                dream homes.</strong></p>
                        <p>We provide comprehensive interior detailing, space planning and consultation, from concept
                            to completion.</p> -->
                        {!! $blocks['INTRODUCE-GENERAL'][0]->content !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection