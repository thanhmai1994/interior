<?php
return [
    'title' => 'Category',
    'category' => 'Category',
    'project_category' => 'Category',
    'list' => 'List',
    'create' => 'Create Category',
    'edit' => 'Edit Category',
    'form' => [
        'name' => 'Name',
        'photo_translation' => 'Photo',
        'description' => 'Description',
        'type' => 'Type'
    ],
    'table' => [
        'id' => '#',
        'name' => 'Name',
        'type' => 'Type',
        'action' => 'Actions',
    ]
];