<?php

return [
    'designs' => 'DESIGNS',
    'attr' => [
        'PROJECT' => 'Project',
        'DESIGN' => 'Design'
    ]
];