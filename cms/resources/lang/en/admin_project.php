<?php
return [
    'title' => 'Project - Design',
    'list' => 'List',
    'create' => 'Create Project - Design',
    'edit' => 'Edit Project - Design',
    'project' => 'Project - Design',
    'form' => [
        'category' => 'Category',
        'name' => 'Name',
        'position' => 'Position',
        'description' => 'Description',
        'content' => 'Content',
        'image' => 'Image',
        'banner' => 'Large Image',
        'active' => 'Active?',
        'publish_at' => 'Published at',
        'is_top' => 'Top Project',
        'video' => 'Video URL',
        'category' => 'Category'
    ],
    'table' => [
        'id' => '#',
        'name' => 'Name',
        'active' => 'Active',
        'is_top' => 'Top',
        'publish_at' => 'Published at',
        'created_at' => 'Created at',
        'action' => 'Actions',
        'category' => 'Category',
        'type' => 'Type'
    ],
    'attr' => [
        'active' => 'Active',
        'un_active' => 'In-active'
    ]
];
