<?php

return [
    'projects' => 'PROJECTS',
    'attr' => [
        'PROJECT' => 'Project',
        'DESIGN' => 'Design'
    ]
];