<?php
return [
    'title' => 'System',

    'list' => 'Config system',

    'system' => 'system',

    'form' => [
        'contact_email' => 'Email system (The email will receive contact)',
        'email' => 'Email',
        'location' => 'Location',
        'phone' => 'Phone top',
        'phone_bottom' => 'Phone bottom',
        'address' => 'Address',
        'address_show_room' => 'Address show room',
        'fax' => 'Fax',
        'google_analytic' => 'Google analytic',
        'chat_script' => 'Chat script',

        'facebook' => 'Facebook',
        'youtube' => 'Youtube',
        'google' => 'Google plus',
        'twitter' => 'Twitter',
        'likedin' => 'Liked In',
        'instagram' => 'Instagram',

        'website_title' => 'Website title',
        'website_description' => 'Website description',
        'website_keywords' => 'Website keywords',
        'website_logo' => 'Website logo'
    ],
    'website_info' => 'Website info',
    'social' => 'Social'
];