<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProjectTranslation extends Model
{
    protected $table = "project_translation";

    protected $fillable = [
        'name',
        'slug',
        'description',
        'content'
    ];

    public $timestamps = false;
}
