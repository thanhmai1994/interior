<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\MediaTrait;
use App\Traits\TranslatableExtendTrait;
use App\Traits\SlugTranslationTrait;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Carbon\Carbon;

class Product extends Model
{
    use \Dimsav\Translatable\Translatable, TransformableTrait, MediaTrait, SlugTranslationTrait;

    protected $table = 'product';

    protected $fillable = [
        'active'
    ];

    public $timestamps = false;

    public $translatedAttributes = [
        'name',
        'slug',
        'description',
        'content'
    ];

    public function scopeActive($query)
    {
        return $query->where('active', 1);
    }
}
