<?php

namespace App\Models;

use App\Traits\SlugTranslationTrait;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use App\Traits\TranslatableExtendTrait;
use Prettus\Repository\Traits\TransformableTrait;

class ProjectCategory extends Model implements Transformable
{
    use \Dimsav\Translatable\Translatable, TransformableTrait, SlugTranslationTrait,TranslatableExtendTrait;

    const PROJECT = 'PROJECT';
    const DESIGN = 'DESIGN';

    protected $table = 'project_category';

    protected $fillable = [
        'id',
        'type',
        'created_at',
        'updated_at'
    ];

    public $translatedAttributes = ['slug', 'name', 'photo_translation', 'description'];

    public $slug_from_source = 'name';

    public static function types($key = null)
    {
        $arr = [
            self::PROJECT => trans('project.attr.PROJECT'),
            self::DESIGN => trans('project.attr.DESIGN')
        ];
        return $key ? $arr[$key] : $arr;
    }

    public function projects()
    {
        return $this->hasMany(Project::class, "project_category_id");
    }

    public function designs()
    {
        return $this->hasMany(Project::class, "project_category_id");
    }
}
