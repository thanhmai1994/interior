<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\MediaTrait;
use App\Traits\TranslatableExtendTrait;
use App\Traits\SlugTranslationTrait;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Carbon\Carbon;

class Project extends Model
{
    use \Dimsav\Translatable\Translatable, TransformableTrait, MediaTrait, SlugTranslationTrait;

    protected $table = 'project';

    protected $fillable = [
        'project_category_id',
        'position'
    ];

    public $timestamps = false;

    public $translatedAttributes = [
        'name',
        'slug',
        'description',
        'content'
    ];

    public function category()
    {
        return $this->belongsTo(ProjectCategory::class, 'project_category_id');
    }
}
