<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class ProjectCategoryTranslation extends Model
{
    protected $table = 'project_category_translation';

    protected $fillable = [
        'slug',
        'name',
        'photo_translation',
        'description'
    ];

    public $timestamps = false;
}
