<?php

namespace App\Repositories;

use App\Models\ObjectMedia;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\ProductRepository;
use App\Models\Product;
use App\Traits\UploadPhotoTrait;
use App\Validators\ProductValidator;

class ProductRepositoryEloquent extends BaseRepository implements ProductRepository
{
    use UploadPhotoTrait;

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Product::class;
    }

    public function validator()
    {
        return ProductValidator::class;
    }

    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function datatable()
    {
        return $this->model->select('*')->withTranslation();
    }

    public function create(array $input)
    {

        $input['active'] = !empty($input['active']) ? 1 : 0;

        $model = $this->model->create($input);

        if (!empty($input['photos'])) {
            $model->createMedia($input['photos']);
        }

        $model->updateSlugTranslation();

        return $model;

    }

    public function update(array $input, $id)
    {
        $model = $this->model->findOrFail($id);

        $input['active'] = !empty($input['active']) ? 1 : 0;

        $model->update($input);

        if (!empty($input['photos'])) {
            $model->createMedia($input['photos']);
        }

        if (!empty($input['delete_photos'])) {
            ObjectMedia::whereIn('id', $input['delete_photos'])->delete();
        }

        $model->updateSlugTranslation();

        return $model;
    }

    public function delete($id)
    {
        $model = $this->model->findOrFail($id);

        $model->medias()->delete();

        $model->delete();
    }

    public function sortPhoto($positions)
    {
        $arr = explode('&', $positions);
        if ($arr && count($arr)) {
            for ($i = 0; $i < count($arr); $i++) {
                $_arr = explode('=', $arr[$i]);
                ObjectMedia::where('id', $_arr[0])->update(['position' => $_arr[1]]);
            }
        }
    }

    public function otherGallery($id, $limit = 8)
    {
        return $this->model
            ->withTranslation()
            ->where('id', '!=', $id)
            ->limit($limit)
            ->get();
    }

    public function findBySlug($slug)
    {
        $locale = \App::getLocale();
        return $this->model
            ->whereTranslation('slug', $slug, $locale)
            ->with('translations')
            ->firstOrFail();
    }

    public function getProduct($paginate = 0)
    {
        $model = $this->model->select('*')->active()->withTranslation();

        if ($paginate) {
            return $model->orderBy('created_at', 'desc')->paginate($paginate);
        }

        return $model->orderBy('created_at', 'desc')->get();
    }
}
