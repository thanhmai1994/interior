<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\LoanManagementRepository;
use App\Models\LoanManagement;
use App\Validators\LoanManagementValidator;

/**
 * Class LoanManagementRepositoryEloquent
 * @package namespace App\Repositories;
 */
class LoanManagementRepositoryEloquent extends BaseRepository implements LoanManagementRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return LoanManagement::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {
        return LoanManagementValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function datatable()
    {
        return $this->model->select('*')->orderBy('id', 'desc');
    }

    public function create(array $input)
    {
        $input['active'] = !empty($input['active']) ? 1 : 0;
        
        $input['amount'] = convertStringToNumber($input['amount']);

        $input['monthly_payment'] = convertStringToNumber($input['monthly_payment']);

        $loan_management = $this->model->create($input);

        return $loan_management;
    }

    public function update(array $input, $id)
    {
        $loan_management = $this->model->findOrFail($id);

        $input['active'] = !empty($input['active']) ? 1 : 0;

        $input['amount'] = convertStringToNumber($input['amount']);

        $input['monthly_payment'] = convertStringToNumber($input['monthly_payment']);

        $loan_management->update($input);

        return $loan_management;
    }

    public function delete($id)
    {
        $loan_management = $this->model->findOrFail($id);

        $loan_management->delete();

        return true;
    }
}
