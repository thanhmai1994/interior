<?php

namespace App\Repositories;

use App\Models\ObjectMedia;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Models\Project;
use App\Traits\UploadPhotoTrait;
use App\Validators\ProjectValidator;

class ProjectRepositoryEloquent extends BaseRepository implements ProjectRepository
{
    use UploadPhotoTrait;

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Project::class;
    }

    public function validator()
    {
        return ProjectValidator::class;
    }

    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function datatable()
    {
        return $this->model->select('*')->withTranslation();
    }

    public function create(array $input)
    {

        $model = $this->model->create($input);

        if (!empty($input['photos'])) {
            $model->createMedia($input['photos']);
        }

        $model->updateSlugTranslation();

        return $model;

    }

    public function update(array $input, $id)
    {
        $model = $this->model->findOrFail($id);

        $model->update($input);

        if (!empty($input['photos'])) {
            $model->createMedia($input['photos']);
        }

        if (!empty($input['delete_photos'])) {
            ObjectMedia::whereIn('id', $input['delete_photos'])->delete();
        }

        $model->updateSlugTranslation();

        return $model;
    }

    public function delete($id)
    {
        $model = $this->model->findOrFail($id);

        $model->medias()->delete();

        $model->delete();
    }

    public function sortPhoto($positions)
    {
        $arr = explode('&', $positions);
        if ($arr && count($arr)) {
            for ($i = 0; $i < count($arr); $i++) {
                $_arr = explode('=', $arr[$i]);
                ObjectMedia::where('id', $_arr[0])->update(['position' => $_arr[1]]);
            }
        }
    }

    public function otherGallery($id, $limit = 8)
    {
        return $this->model
            ->withTranslation()
            ->where('id', '!=', $id)
            ->limit($limit)
            ->get();
    }

    public function findBySlug($slug_category, $slug)
    {
        $locale = \App::getLocale();
        return $this->model
            ->whereHas('category', function($query) use ($slug_category){
                $query->whereTranslation('slug', $slug_category);
            })
            ->whereTranslation('slug', $slug, $locale)
            ->with('translations')
            ->firstOrFail();
    }
}
