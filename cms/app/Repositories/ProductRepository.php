<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ProductRepository
 * @package namespace App\Repositories;
 */
interface ProductRepository extends RepositoryInterface
{

    public function datatable();

    public function sortPhoto($positions);

    public function otherGallery($id, $limit = 8);

    public function findBySlug($slug);

    public function getProduct($paginate = 0);

}
