<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

interface ProjectCategoryRepository extends RepositoryInterface
{
    public function datatable();

    public function findBySlug($slug);
}
