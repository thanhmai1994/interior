<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ProjectRepository
 * @package namespace App\Repositories;
 */
interface ProjectRepository extends RepositoryInterface
{

    public function datatable();

    public function sortPhoto($positions);

    public function otherGallery($id, $limit = 8);

    public function findBySlug($slug_category, $slug);

}
