<?php

namespace App\Repositories;

use App\Models\ProjectCategory;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;

class ProjectCategoryRepositoryEloquent extends BaseRepository implements ProjectCategoryRepository
{
    public function model()
    {
        return ProjectCategory::class;
    }

    public function validator()
    {
        //
    }

    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function datatable()
    {
        return $this->model->select('*')->withTranslation();
    }

    public function create(array $input)
    {
        $model = $this->model->create($input);
        $model->updateSlugTranslation();
        return $model;
    }

    public function update(array $input, $id)
    {
        $model = $this->model->find($id);
        $model->update($input);
        $model->updateSlugTranslation();
        return $model;
    }

    public function findBySlug($slug)
    {
        $locale = \App::getLocale();
        return $this->model
            ->whereTranslation('slug', $slug, $locale)
            ->with('translations')
            ->firstOrFail();
    }
    
}
