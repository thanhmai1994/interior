<?php

namespace App\Http\Controllers\Frontend;

use App\Models\FaqCategory;
use Validator;
use App\Helper\TranslateUrl;
use App\Http\Requests\Frontend\ContactRequest;
use App\Http\Requests\Frontend\FaqsRequest;
use App\Http\Requests\Frontend\RfpRequest;
use App\Jobs\SubscribeJob;
use App\Mail\EMail;
use App\Mail\SendContactEmail;
use App\Mail\SendContactEmailConfirm;
use App\Models\Career;
use App\Models\PageBlock;
use App\Models\Ward;
use App\Repositories\CareerRepository;
use App\Repositories\ContactRepository;
use App\Repositories\NewsRepository;
use App\Repositories\PageRepository;
use App\Http\Controllers\Controller;
use App\Repositories\PartnerRepository;
use App\Repositories\ProductRepository;
use App\Repositories\FaqQuestionRepository;
use App\Repositories\FaqCategoryRepository;
use App\Repositories\BusinessRepository;
use App\Repositories\CountryRepository;
use App\Repositories\BrochuresRepository;
use App\Repositories\RfpRepository;
use App\Repositories\SliderRepository;
use App\Repositories\SystemRepository;
use App\Repositories\TeamRepository;
use App\Repositories\AchievementsRepository;
use App\Repositories\SharedValueRepository;
use Illuminate\Http\Request;
use Breadcrumb;
use Carbon\Carbon;
use function GuzzleHttp\default_ca_bundle;
use Mail;

# Models
use App\Models\City;
use App\Models\AddressCategory;
use App\Models\Address;
use App\Models\Slider;
use App\Models\ProjectCategory;

# Transformers
use League\Fractal\Resource\Collection;

use League\Fractal\Manager;
use App\Transformers\AddressTransformer;


class PageController extends Controller
{
    protected $page;
    protected $product;
    protected $news;
    protected $partner;
    protected $contact;
    protected $rfp;
    protected $faqquest;
    protected $faqcate;
    protected $business;
    protected $country;
    protected $achievements;
    protected $shared_value;
    private $slider;
    private $brochures;
    private $team;
    private $system;
    private $career;

    protected $manager;
    protected $address_transformer;

    public function __construct(
        PageRepository $page,
        ProductRepository $product,
        NewsRepository $news,
        PartnerRepository $partner,
        ContactRepository $contact,
        RfpRepository $rfp,
        FaqQuestionRepository $faqquest,
        FaqCategoryRepository $faqcate,
        BusinessRepository $business,
        CountryRepository $country,
        SliderRepository $slider,
        BrochuresRepository $brochures,
        TeamRepository $team,
        SystemRepository $system,
        AchievementsRepository $achievements,
        SharedValueRepository $shared_value,
        CareerRepository $career,
        Manager $manager,
        AddressTransformer $address_transformer
    )
    {
        $this->page = $page;
        $this->product = $product;
        $this->news = $news;
        $this->partner = $partner;
        $this->contact = $contact;
        $this->rfp = $rfp;
        $this->faqquest = $faqquest;
        $this->faqcate = $faqcate;
        $this->business = $business;
        $this->country = $country;
        $this->slider = $slider;
        $this->brochures = $brochures;
        $this->team = $team;
        $this->system = $system;
        $this->achievements = $achievements;
        $this->shared_value = $shared_value;
        $this->career = $career;

        $this->manager = $manager;
        $this->address_transformer = $address_transformer;
    }

    public function index()
    {

        $page = $this->page->findBySlug('/');

        $blocks = [];

        if ($page->parentBlocks->count()) {
            $blocks = $page->parentBlocks->groupBy('code');
        }

        if (request()->get('debug')) {
            dd($blocks);
        }

        foreach ($page->translations as $translation) {
            TranslateUrl::addWithLink($translation->locale, "/{$translation->locale}");
        }

        $metadata = $page->meta;

        $slider = Slider::get();
        
        if (view()->exists(THEME_PATH_VIEW . ".{$page->theme}")) {
            return view(THEME_PATH_VIEW . ".{$page->theme}", compact('page', 'blocks', 'metadata', 'slider'));
        }

        abort(404);
    }

    public function show($slug)
    {
        $with = [
            'translations',
            'parentBlocks',
            'parentBlocks.children',
            'meta'
        ];

        $page = $this->page->findBySlug($slug, $with);

        $blocks = [];

        if ($page->parentBlocks->count()) {
            $blocks = $page->parentBlocks->groupBy('code');//->toArray();
        }

        if (request()->get('debug')) {
            dd($blocks);
        }

        foreach ($page->translations as $translation) {
            $url = $translation->slug ? $translation->slug : COMING_SOON;
            TranslateUrl::addWithLink($translation->locale, "/{$translation->locale}/{$url}");
        }

        $metadata = $page->meta;

        if (!$metadata || !$metadata->title) {
            $metadata = (object)[
                'title' => $page->title,
                'description' => $page->description,
                'key_word' => '3forcom'
            ];
        }

        if ($page->parent_id) {
            $parent = $page->parent;
            if ($parent) {
                Breadcrumb::add($parent->title, $parent->url);
            }
        }

        Breadcrumb::add($page->title);

        if (view()->exists(THEME_PATH_VIEW . ".{$page->theme}")) {

            $with = [];

            if($page->theme == 'product') {

                $product = $this->product->getProduct($paginate = 9);

                $with = [
                    'product' => $product
                ];
            }

            if($page->theme == 'project') {

                $project_category = ProjectCategory::where('type', 'PROJECT')->withTranslation()->get();

                $with = [
                    'project_category' => $project_category
                ];
            }

            if($page->theme == 'design') {

                $design_category = ProjectCategory::where('type', 'DESIGN')->withTranslation()->get();

                $with = [
                    'design_category' => $design_category
                ];
            }

            return view(THEME_PATH_VIEW . ".{$page->theme}", compact('page', 'blocks', 'metadata', 'slug'))->with($with);

        }

        abort(404);
    }

    public function subscribe()
    {
        $email = request()->input('email');
        $this->dispatch(new SubscribeJob($email));
        if (request()->isJson() || request()->wantsJson() || request()->ajax()) {
            return restSuccess(trans('message.subscribe_success'));
        }
        return redirect()->back()->with('success',trans('message.subscribe_success'));
    }

    public function storeContact(ContactRequest $request)
    {
        $input = $request->all();

        $this->contact->create($input);

        // if (\App::environment('production')) {

        //     $locale = \App::getLocale();

        //     $system_email = \System::content('contact_email', env('CONTACT_EMAIL'));

        //     \Mail::to($input['email'])->send(new SendContactEmailConfirm($input, $locale));

        //     if ($system_email) {
        //         \Mail::to($system_email)->send(new SendContactEmail($input, $locale));
        //     }
        // }

        return redirect()->back()->with('success', trans('message.contact_sent_success'));
    }

    public function storeRfp(RfpRequest $request)
    {
        $input = $request->all();
        if ($request->hasFile('file_name')) {
            $file = $request->file('file_name');
            $file_name = date('dmY-His-') . $file->getClientOriginalName();
            $file->move(public_path('rfp'), $file_name);
            $input['file_name'] = $file_name;
        }
        $input['solution'] = implode(';;', $request->get('solution'));
        $this->rfp->create($input);
        session()->flash('success', trans('message.rfp_sent_success'));
        return redirect()->back();
    }

    public function faqs()
    {
        Breadcrumb::add(trans('menu.investment-consultancy'), trans('routes.page_investment-consultancy'));
        // add breadcrumb
        Breadcrumb::add(trans('menu.faqs'));

        $main_cate = $this->faqcate->faqs()->first();

        $other_cate = $this->faqcate->faqs()->where('id', '<>', $main_cate->id);

        return view('frontend.faqs', compact('main_cate', 'other_cate'));
    }

    public function storeFaqquest(FaqsRequest $request)
    {
        $input = $request->all();

        $this->faqquest->create($input);

        $system_email = \System::content('contact_email', env('CONTACT_EMAIL'));

        if ($system_email) {

            Mail::send('emails.question_customer', compact('input'), function ($message) use ($system_email) {
                $message->to($system_email)
                    ->subject(trans('emails.new_contact_email') . " " . date('H:i d:m:Y'));
            });
        }

        session()->flash('success', trans('message.request_faqs_sent_success'));

        return redirect()->back();
    }

    public function joinUsWhy($page_slug, $bolck_slug, $page_id)
    {
        $page = $this->page->findBySlug($page_slug);
        $blocks = [];
        if ($page->parentBlocks->count()) {
            $blocks = $page->parentBlocks->groupBy('code');
        }

        $page_block = PageBlock::find($page_id);

        return view('themes.join-us-workspace-why-easy-credit-detail',compact('page_block','blocks','page'));
    }

    public function getCareerDetail($page_slug, $career_slug)
    {
        $page = $this->page->findBySlug($page_slug);
        $blocks = [];
        if ($page->parentBlocks->count()) {
            $blocks = $page->parentBlocks->groupBy('code');
        }

        $career = $this->career->findBySlug($career_slug);

        return view('themes.apply-career', compact('blocks', 'career'));
    }

    public function searchNewsPromitions(Request $request, $slug)
    {

        $with = [
            'translations',
            'parentBlocks',
            'parentBlocks.children',
            'meta'
        ];

        $page = $this->page->findBySlug($slug, $with);

        $blocks = [];

        if ($page->parentBlocks->count()) {
            $blocks = $page->parentBlocks->groupBy('code');//->toArray();
        }

        if (request()->get('debug')) {
            dd($blocks);
        }

        foreach ($page->translations as $translation) {
            $url = $translation->slug ? $translation->slug : COMING_SOON;
            TranslateUrl::addWithLink($translation->locale, "/{$translation->locale}/{$url}");
        }

        $metadata = $page->meta;

        if (!$metadata || !$metadata->title) {
            $metadata = (object)[
                'title' => $page->title,
                'description' => $page->description,
                'key_word' => '3forcom'
            ];
        }

        if ($page->parent_id) {
            $parent = $page->parent;
            if ($parent) {
                Breadcrumb::add($parent->title, $parent->url);
            }
        }

        Breadcrumb::add($page->title);

        if (view()->exists(THEME_PATH_VIEW . ".{$page->theme}")) {

            $with = [];

            if($page->theme == 'news-promitions') {
                $news_top = $this->news->topNews($limit = 3, $is_top = true);

                $news_video = $this->news->topNews($limit = 2, $video = true);

                $news = $this->news->topNews($is_top = false);

                $with = [
                    'news_top' => $news_top,
                    'news_video' => $news_video,
                    'news' => $news
                ];
            }

            $input = $request->only('key');

            $result = $this->news->searchNewsPromitions($input['key']);

            return view(THEME_PATH_VIEW . ".{$page->theme}", compact('page', 'blocks', 'metadata', 'slug', 'result'))->with($with);
        }

        return view('themes.apply-career', compact('page','blocks','career'));
    }

    public function getWard(Request $request)
    {
        $district_id = $request->get('district_id');
        return Ward::where('district_id',$district_id)->get()->map(function ($item){
            $intval = intval($item->name);
            return [
                'id'=>$item->id,
                'name'=>$intval ? "Phường $intval" : $item->name
            ];
        });
    }

    public function urlJob(Request $request)
    {
        $data = $request->only('value_job', 'value_combo');
        
        $loan_job = LoanJob::find($data['value_job']);

        $combo = Combo::find((int)$data['value_combo']);
        
        $loan_income_type_id = null;
        if(count($combo))
        {
            $loan_income_type_id = $combo->loan_income_type_id;
        }

        $loan_general = LoanGeneral::where('loan_job_id', $loan_job->id)->where('loan_income_type_id', $loan_income_type_id)->first();

        $loan_setting = null;
        if(count($loan_general))
        {
            $loan_setting = $loan_general->loanSetting;
        }

        return restSuccess('Data', $loan_setting, 200);
    }

    public function urlLoan(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "job_id"        => "required",
            "combo_id"      => "required",
            "money"         => "required",
            "month"         => "required",
            "name"          => "required",
            "phone"         => "required|regex:/^\+?[^a-zA-Z]{5,}$/",
            "email"         => "required|email"
        ]);
        
        if($validator->fails())
        {
            return restFail($validator->errors());
        }

        $input = $request->all();

        $pay = calculatorLoan($input['money'], $input['interest_rate'], $input['month'], $input['coefficient']);

        $data= [
            'name'              => $input['name'],
            'phone'             => $input['phone'],
            'email'             => $input['email'],
            'city_id'           => $input['city'],
            'job_id'            => $input['job_id'],
            'combo_id'          => $input['combo_id'],
            'duration'          => $input['month'],
            'amount'            => $input['money'],
            'monthly_payment'   => $pay
        ];

        LoanManagement::create($data);

        return restSuccess();

    }

    public function urlGetCombo(Request $request)
    {
        $input = $request->all();

        $id_job = $input['job_val_get_combo'];

        $job = LoanJob::findOrFail($id_job);

        // Get document unique id from combo collection
        $combo_id = $job->combos->pluck('id')->toArray();
        $id_document = \DB::table('document_combo')->whereIn('combo_id', $combo_id)->get()->pluck('document_id')->toArray();
        $id_document = array_unique($id_document);

        // Combo, Document of combo collection
        $combo = $job->combos;
        $document = Document::whereIn('id', $id_document)->get();

        $thead = '';
        $tbody = '';
        $button = '';
        if(!empty($combo)) {
            foreach($combo as $key) {
                $thead .= '<th>'. $key->name . '</th>';
                $button .= '<td>
                                <div class="btn-wrap">
                                    <button class="btn btn-primary btn-sm btn-shadow choose_doc" data-val="'. $key->id .'">Chọn</button>
                                </div>
                            </td>';
                
            }
        }
        

        if(!empty($document) && !empty($combo)) {
            foreach($document as $key) {
                $tbody .= '<tr>
                                <th>'. $key->name .'</th>';
                    
                                foreach($combo as $key2) {
                                    $tbody .= '<td>';
                                        $array_document = $key2->documents->pluck('id')->toArray();
                                        if(in_array($key->id, $array_document)) {
                                            $tbody .= '<img src="/assets/images/logo-small.svg" alt="">';
                                        }
                                    $tbody .= '</td>';
                                }
                $tbody .= '</tr>';
            }
        }

        $html = '<table>
                    <thead>
                        <tr>
                            <th>Giấy tờ hợp lệ</th>
                            '. $thead .'
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            '. $tbody .'
                        </tr>
                        
                        <tr class="button">
                            <th></th>
                            '. $button .'
                        </tr>
                    </tbody>
                </table>';

        return $html;
    }

}
