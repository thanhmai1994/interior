<?php

namespace App\Http\Controllers\Frontend;

use App\Helper\TranslateUrl;
use App\Http\Controllers\Controller;
use App\Repositories\ProjectRepository;
use App\Repositories\ProjectCategoryRepository;
use App\Models\ProjectCategory;
use Breadcrumb;

class ProjectController extends Controller
{
    protected $project;

    public function __construct(ProjectRepository $project, ProjectCategoryRepository $project_category)
    {
        $this->project = $project;
        $this->project_category = $project_category;
    }

    public function show($slug_category)
    {

        $project_category = $this->project_category->findBySlug($slug_category);

        $project_category_all = ProjectCategory::where('type', 'PROJECT')->get();

        // translation
        foreach ($project_category->translations as $translation) {
            TranslateUrl::add($translation->locale, 'routes.project_show', ['slug' => $translation->slug]);
        }

        // add breadcrumb
        Breadcrumb::add(trans('project.projects'), trans('breadcrumb.project'));
        Breadcrumb::add($project_category->name, route('project.show', $project_category->slug));

        return view('frontend.project.show', compact('project_category', 'project_category_all'));
    }

    public function detail($slug_category, $slug)
    {
        $project = $this->project->findBySlug($slug_category, $slug);

        $project_category = $this->project_category->find($project->id);

        $project_category_all = ProjectCategory::where('type', 'PROJECT')->get();

        $metadata = $project->meta;

        if (!$metadata || !$metadata->title) {
            $metadata = (object)[
                'title' => $project->name,
                'description' => $project->description,
                'key_word' => $project->name
            ];
        }

        // translation
        foreach ($project->translations as $translation) {
            TranslateUrl::add($translation->locale, 'routes.project_show', ['slug' => $translation->slug]);
        }

        // add breadcrumb
        Breadcrumb::add(trans('project.projects'), trans('breadcrumb.project'));
        Breadcrumb::add($project_category->name, route('project.show', $project_category->slug));
        Breadcrumb::add($project->name);

        return view('frontend.project.detail', compact('project', 'metadata', 'project_category_all'));
    }

    public function showDesign($slug_category)
    {

        $design_category = $this->project_category->findBySlug($slug_category);

        $design_category_all = ProjectCategory::where('type', 'DESIGN')->get();

        // translation
        foreach ($design_category->translations as $translation) {
            TranslateUrl::add($translation->locale, 'routes.design_show', ['slug' => $translation->slug]);
        }

        // add breadcrumb
        Breadcrumb::add(trans('design.designs'), trans('breadcrumb.design'));
        Breadcrumb::add($design_category->name, route('design.show', $design_category->slug));

        return view('frontend.design.show', compact('design_category', 'design_category_all'));
    }

    public function detailDesign($slug_category, $slug)
    {
        $design = $this->project->findBySlug($slug_category, $slug);

        $design_category = $this->project_category->find($design->id);

        $design_category_all = ProjectCategory::where('type', 'DESIGN')->get();

        $metadata = $design->meta;

        if (!$metadata || !$metadata->title) {
            $metadata = (object)[
                'title' => $design->name,
                'description' => $design->description,
                'key_word' => $design->name
            ];
        }

        // translation
        foreach ($design->translations as $translation) {
            TranslateUrl::add($translation->locale, 'routes.design_show', ['slug' => $translation->slug]);
        }

        // add breadcrumb
        Breadcrumb::add(trans('design.designs'), trans('breadcrumb.design'));
        Breadcrumb::add($design_category->name, route('design.show', $design_category->slug));
        Breadcrumb::add($design->name);

        return view('frontend.design.detail', compact('design', 'metadata', 'design_category_all'));
    }

}
