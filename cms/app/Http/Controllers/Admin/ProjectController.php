<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helper\Breadcrumb;
use App\Models\Project;
use App\Repositories\ProjectRepository;
use Yajra\DataTables\Facades\DataTables;
use App\Http\Requests\Admin\ProjectCreateRequest;

use App\Models\ProjectCategory;


class ProjectController extends Controller
{
    protected $project;

    public function __construct(ProjectRepository $project)
    {
        $this->project = $project;
    }
    
    public function index()
    {
        Breadcrumb::title(trans('admin_project.title'));

        return view('admin.project.index');
    }

    public function create()
    {
        Breadcrumb::title(trans('admin_project.create'));

        $project_category = ProjectCategory::get();

        return view('admin.project.create_edit', compact('project_category'));
    }

    public function store(ProjectCreateRequest $request)
    {
        $input = $request->all();

        $this->project->create($input);

        session()->flash('success', trans('admin_message.created_successful', ['attr' => trans('admin_project.project')]));

        return redirect()->route('admin.project.index');
    }

    public function datatable()
    {
        $data = $this->project->datatable();

        return Datatables::of($data)
            ->addColumn(
                'translations',
                function ($data) {
                    return $data->name;
                }
            )
            ->addColumn(
                'category',
                function ($data) {
                    return !empty($data->category) ? $data->category->name : '';
                }
            )
            ->addColumn(
                'type',
                function ($data) {
                    return !empty($data->category) ? $data->category->type : '';
                }
            )
            ->addColumn(
                'action', function ($data) {
                return view('admin.layouts.partials.table_button')->with(
                    [
                        'link_edit' => route('admin.project.edit', $data->id),
                        'link_delete' => route('admin.project.destroy', $data->id),
                        'id_delete' => $data->id
                    ]
                )->render();
            }
            )
            ->escapeColumns([])
            ->make(true);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        Breadcrumb::title(trans('admin_project.edit'));

        $project = $this->project->find($id);

        $project_category = ProjectCategory::get();

        return view('admin.project.create_edit', compact('project', 'project_category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();

        $this->project->update($input, $id);

        session()->flash('success', trans('admin_message.updated_successful', ['attr' => trans('admin_project.project')]));

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->project->delete($id);

        session()->flash('success', trans('admin_message.deleted_successful', ['attr' => trans('admin_project.project')]));

        return redirect()->back();
    }

    public function sortPhoto(Request $request)
    {
        $positions = $request->input('positions');
        $this->project->sortPhoto($positions);
        return restSuccess('Success');
    }
}