<?php

namespace App\Http\Controllers\Admin;

use App\Helper\Breadcrumb;
use App\Repositories\AddressCategoryRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;

class AddressCategoryController extends Controller
{
    protected $address_category;

    public function __construct(AddressCategoryRepository $address_category)
    {
        $this->address_category = $address_category;
    }

    public function index()
    {
        Breadcrumb::title('Address Category');
        return view('admin.address_category.index');
    }

    public function datatable()
    {
        $data = $this->address_category->datatable();

        return DataTables::of($data)
            ->addColumn(
                'translations',
                function ($data) {
                    return $data->name;
                }
            )
            ->addColumn(
                'action',
                function ($data) {
                    return view('admin.layouts.partials.table_button')->with(
                        [
                            'link_edit' => route('admin.address.category.edit', $data->id),
                            'link_delete' => route('admin.address.category.destroy', $data->id),
                            'id_delete' => $data->id
                        ]
                    )->render();
                }
            )
            ->escapeColumns([])
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        Breadcrumb::title('Create Address Category');

        return view('admin.address_category.create_edit');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $this->address_category->create($input);

        session()->flash('success', trans('admin_message.created_successful', ['attr' => 'Address Category']));

        return redirect()->route('admin.address.category.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        Breadcrumb::title('Edit Address Category');

        $address_category = $this->address_category->find($id);

        return view(
            'admin.address_category.create_edit',
            compact(
                'address_category'
            )
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();

        $this->address_category->update($input, $id);

        session()->flash('success', trans('admin_message.updated_successful', ['attr' => 'Address Category']));

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->address_category->delete($id);

        session()->flash('success', trans('admin_message.deleted_successful', ['attr' => 'Address Category']));

        return redirect()->back();
    }
}
