<?php

namespace App\Http\Controllers\Admin;

use App\Helper\Breadcrumb;
use App\Models\Address;
use App\Repositories\AddressCategoryRepository;
use App\Repositories\AddressRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;
use Excel;

# Requests
use App\Http\Requests\Admin\AddressImportRequest;

class AddressController extends Controller
{
    protected $address;
    protected $address_category;

    public function __construct(AddressRepository $address, AddressCategoryRepository $address_category)
    {
        $this->address = $address;
        $this->address_category = $address_category;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Breadcrumb::title('Address');

        return view('admin.address.index');
    }

    public function datatable()
    {
        $data = $this->address->datatable();
        return DataTables::of($data)
           ->addColumn(
               'category',
               function ($data) {
                   return $data->category->name;
               }
           )
            ->addColumn(
                'translations',
                function ($data) {
                    return $data->title;
                }
            )
            ->editColumn(
                'active',
                function ($data) {
                    return $data->active ? '<span class="label label-success">'.$data->label_active.'</span>' : '<span class="label label-warning">'.$data->label_active.'</span>';
                }
            )
            ->addColumn(
                'action',
                function ($data) {
                    return view('admin.layouts.partials.table_button')->with(
                        [
                        'link_edit' => route('admin.address.edit', $data->id),
                        'link_delete' => route('admin.address.destroy', $data->id),
                        'id_delete' => $data->id
                        ]
                    )->render();
                }
            )
            ->escapeColumns([])
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        Breadcrumb::title('Create Address');

        $categories = $this->address_category->all();

        $city = \App\Models\City::all();

        return view('admin.address.create_edit', compact('categories', 'city'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $this->address->create($input);

        session()->flash('success', trans('admin_message.created_successful', ['attr' => 'Address']));

        return redirect()->route('admin.address.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        Breadcrumb::title('Edit Address');

        $address = $this->address->find($id);

        $categories = $this->address_category->all();

        $metadata = $address->meta;

        $city = \App\Models\City::all();

        return view('admin.address.create_edit', compact('address', 'categories', 'metadata', 'city'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();

        $this->address->update($input, $id);

        session()->flash('success', trans('admin_message.updated_successful', ['attr' => 'Address']));

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->address->delete($id);

        session()->flash('success', trans('admin_message.deleted_successful', ['attr' => 'Address']));

        return redirect()->back();
    }

    public function import(AddressImportRequest $request)
    {
        $input = $request->all();

        $excel = Excel::load($request->file('file')->getRealPath(), function ($reader) {
        })->get()->toArray();
        
        if(Excel::load($request->file('file')->getRealPath(), function ($reader) {})->get()[0]->count() > 1) {
            $heading = Excel::load($request->file('file')->getRealPath(), function ($reader) {
            })->get()[0]->getHeading();

            // Check format excel
            if(is_array($heading) 
            && is_array($this->arrayExcel()) 
            && count($heading) == count($this->arrayExcel()) 
            && array_diff($heading, $this->arrayExcel()) === array_diff($this->arrayExcel(), $heading)){
                foreach ($excel as $key => $row) {
                    $import = $this->address->importExcel($row);
                }
            } else {
                session()->flash('error', 'Excel file not formatted!');
                return redirect()->back();
            }
        } else {
            session()->flash('error', 'Excel file not formatted!');
            return redirect()->back();
        }
        

        if($import == true) 
            session()->flash('success', 'Successful Import');

        return redirect()->back();
    }

    public function arrayExcel()
    {
        return $array = [
            'name',
            'area',
            'city',
            'postal',
            'phone',
            'fax',
            'address',
            ''
        ];
    }
}
