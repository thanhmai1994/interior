<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Helper\Breadcrumb;
use App\Http\Controllers\Controller;
use App\Repositories\LoanManagementRepository;
use Yajra\DataTables\Facades\DataTables;
use App\Models\LoanManagement;
use App\Models\LoanJob;
use App\Models\Combo;
use App\Models\City;

class LoanManagementController extends Controller
{

    protected $loan_management;

    public function __construct(LoanManagementRepository $loan_management)
    {
        $this->loan_management = $loan_management;
    }

    public function index()
    {
        return view('admin.loan_management.index');
    }

    public function datatable()
    {
        $data = $this->loan_management->datatable();

        return DataTables::of($data)
            ->editColumn(
                'active',
                function ($data) {
                    return $data->active ? '<span class="label label-success">'.$data->label_active.'</span>' : '<span class="label label-warning">'.$data->label_active.'</span>';
                }
            )
            ->addColumn(
                'action',
                function ($data) {
                    return view('admin.layouts.partials.table_button')->with(
                        [
                            'link_edit' => route('admin.loan.management.edit', $data->id),
                            'link_delete' => route('admin.loan.management.destroy', $data->id),
                            'id_delete' => $data->id
                        ]
                    )->render();
                }
            )
            ->escapeColumns([])
            ->make(true);
    }

    public function edit($id)
    {
        Breadcrumb::title('Edit');

        $loan_management = $this->loan_management->find($id);

        $loan_job = LoanJob::select('id', 'name')->active()->get();

        $combo = Combo::select('id', 'name')->active()->get();

        $city = City::all();

        return view('admin.loan_management.create_edit', compact('loan_management', 'loan_job', 'combo', 'city'));
    }

    public function update(Request $request, $id)
    {
        $input = $request->all();

        $this->loan_management->update($input, $id);

        session()->flash('success', trans('admin_message.updated_successful', ['attr' => 'Loan Management']));

        return redirect()->back();
    }

    public function destroy($id)
    {
        $this->loan_management->delete($id);

        session()->flash('success', trans('admin_message.deleted_successful', ['attr' => 'Loan Management']));

        return redirect()->back();
    }
}
