<?php

namespace App\Http\Controllers\Admin;

use App\Helper\Breadcrumb;
use App\Repositories\ProjectCategoryRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;
use App\Models\ProjectCategory;

class ProjectCategoryController extends Controller
{
    protected $project_category;

    public function __construct(ProjectCategoryRepository $project_category)
    {
        $this->project_category = $project_category;
    }

    public function index()
    {
        Breadcrumb::title(trans('admin_project_category.title'));
        return view('admin.project_category.index');
    }

    public function datatable()
    {
        $data = $this->project_category->datatable();

        return DataTables::of($data)
            ->addColumn(
                'translations',
                function ($data) {
                    return $data->name;
                }
            )
            ->addColumn(
                'action',
                function ($data) {
                    return view('admin.layouts.partials.table_button')->with(
                        [
                            'link_edit' => route('admin.project.category.edit', $data->id),
                            'link_delete' => route('admin.project.category.destroy', $data->id),
                            'id_delete' => $data->id
                        ]
                    )->render();
                }
            )
            ->escapeColumns([])
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $types = ProjectCategory::types();

        Breadcrumb::title(trans('admin_project_category.create'));

        return view('admin.project_category.create_edit', compact('types'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $this->project_category->create($input);

        session()->flash('success', trans('admin_message.created_successful', ['attr' => trans('admin_project_category.project_category')]));

        return redirect()->route('admin.project.category.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        Breadcrumb::title(trans('admin_project_category.edit'));

        $project_category = $this->project_category->find($id);

        $types = ProjectCategory::types();

        return view('admin.project_category.create_edit',compact('project_category', 'types'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();

        $this->project_category->update($input, $id);

        session()->flash('success', trans('admin_message.updated_successful', ['attr' => trans('admin_project_category.project_category')]));

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->project_category->delete($id);

        session()->flash('success', trans('admin_message.deleted_successful', ['attr' => trans('admin_project_category.project_category')]));

        return redirect()->back();
    }
}
