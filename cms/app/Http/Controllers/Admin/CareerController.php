<?php

namespace App\Http\Controllers\Admin;

use App\Helper\Breadcrumb;
use App\Models\Career;
use App\Repositories\CareerApplyRepository;
use App\Repositories\CareerCategoryRepository;
use App\Repositories\CareerRepository;
use Illuminate\Http\Request;
use App\Models\City;
use App\Models\CareerLevel;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;

class CareerController extends Controller
{
    protected $career;
    protected $apply;
    protected $category;

    public function __construct(
        CareerRepository $career,
        CareerApplyRepository $apply,
        CareerCategoryRepository $category)
    {
        $this->career = $career;
        $this->apply = $apply;
        $this->category = $category;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Breadcrumb::title(trans('admin_career.title'));

        return view('admin.career.index');
    }

    public function datatable()
    {
        $data = $this->career->datatable();
        return DataTables::of($data)
            ->addColumn(
                'translations',
                function ($data) {
                    return $data->name;
                }
            )
            ->editColumn(
                'employer',
                function ($data) {
                    return $data->employer;
                }
            )
            ->editColumn(
                'status',
                function ($data) {
                    return $data->status;
                }
            )
            ->editColumn(
                'is_top',
                function ($data) {
                    return $data->is_top ? '<i class="material-icons col-pink">check</i>' : '<i class="material-icons">more_horiz</i>';
                }
            )
            ->addColumn(
                'num_of_application',
                function ($data) {
                    return $data->applies()->count();
                }
            )
            ->editColumn(
                'expired_date',
                function ($data) {
                    return $data->expired_date_format;
                }
            )
            ->addColumn(
                'action',
                function ($data) {
                    return view('admin.layouts.partials.table_button')->with(
                        [
                            'link_edit' => route('admin.career.edit', $data->id),
                            'link_delete' => route('admin.career.destroy', $data->id),
                            'id_delete' => $data->id
                        ]
                    )->render();
                }
            )
            ->escapeColumns([])
            ->make(true);
    }

    public function application()
    {
        Breadcrumb::title(trans('admin_career.apply'));

        return view('admin.career.application');
    }

    public function applicationDatatable()
    {
        $data = $this->apply->datatable();

        return DataTables::of($data)
            ->addColumn(
                'name', function ($data) {
                return $data->first_name .' '.$data->last_name; //nhatuyendung
            })
            ->addColumn(
                'position', function ($data) {
                return $data->career->name; //career
            })
            ->editColumn(
                'created_at',
                function ($data) {
                    return cvDbTime($data->created_at, DB_TIME, PHP_DATE_TIME);
                }
            )
            ->editColumn(
                'image',
                function ($data) {
                    return "<img src='storage/{$data->image}' style='max-width: 80px;max-height: 60px;' />";
                }
            )
            ->editColumn(
                'attach_file',
                function ($data) {
                    return "<a href='storage/{$data->attach_file}' class='btn btn-primary' target='_blank'>Detail</a>";
                }
            )
            ->addColumn("action", function ($data) {
                return " <a class='btn btn-success btn-detail'>" . trans('button.view') . "<a>";
            })
            ->escapeColumns(['*'])
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        Breadcrumb::title(trans('admin_career.create_career'));

        $statuses = Career::getStatuses();
        $categories = $this->category->all();
        $employer = Career::getEmployer();
        $locations = City::all();
        $levels = CareerLevel::all();

        return view(
            'admin.career.create_edit',
            compact(
                'statuses',
                'employer',
                'categories',
                'locations',
                'levels'
            )
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $this->career->create($input);

        session()->flash('success', trans('admin_message.created_successful', ['attr' => trans('admin_career.career')]));

        return redirect()->route('admin.career.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $career = $this->career->find($id);

        $statuses = Career::getStatuses();
        $employer = Career::getEmployer();
        $categories = $this->category->all();
        $locations = City::all();
        $levels = CareerLevel::all();

        return view(
            'admin.career.create_edit',
            compact(
                'career',
                'statuses',
                'categories',
                'employer',
                'locations',
                'levels'
            )
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();

        $this->career->update($input, $id);

        session()->flash('success', trans('admin_message.updated_successful', ['attr' => trans('admin_career.career')]));

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->career->delete($id);

        session()->flash('success', trans('admin_message.deleted_successful', ['attr' => trans('admin_career.career')]));

        return redirect()->back();
    }
}
