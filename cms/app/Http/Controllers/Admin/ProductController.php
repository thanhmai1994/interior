<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helper\Breadcrumb;
use App\Models\Product;
use App\Repositories\ProductRepository;
use Yajra\DataTables\Facades\DataTables;
use App\Http\Requests\Admin\ProductCreateRequest;


class ProductController extends Controller
{
    protected $product;

    public function __construct(ProductRepository $product)
    {
        $this->product = $product;
    }
    
    public function index()
    {
        Breadcrumb::title(trans('admin_product.title'));

        return view('admin.product.index');
    }

    public function create()
    {
        Breadcrumb::title(trans('admin_product.create'));

        return view('admin.product.create_edit');
    }

    public function store(ProductCreateRequest $request)
    {
        $input = $request->all();

        $this->product->create($input);

        session()->flash('success', trans('admin_message.created_successful', ['attr' => trans('admin_product.product')]));

        return redirect()->route('admin.product.index');
    }

    public function datatable()
    {
        $data = $this->product->datatable();

        return Datatables::of($data)
            ->addColumn(
                'translations',
                function ($data) {
                    return $data->name;
                }
            )
            ->addColumn(
                'action', function ($data) {
                return view('admin.layouts.partials.table_button')->with(
                    [
                        'link_edit' => route('admin.product.edit', $data->id),
                        'link_delete' => route('admin.product.destroy', $data->id),
                        'id_delete' => $data->id
                    ]
                )->render();
            }
            )
            ->escapeColumns([])
            ->make(true);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        Breadcrumb::title(trans('admin_product.edit'));

        $product = $this->product->find($id);

        return view('admin.product.create_edit', compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();

        $this->product->update($input, $id);

        session()->flash('success', trans('admin_message.updated_successful', ['attr' => trans('admin_product.product')]));

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->product->delete($id);

        session()->flash('success', trans('admin_message.deleted_successful', ['attr' => trans('admin_product.product')]));

        return redirect()->back();
    }

    public function sortPhoto(Request $request)
    {
        $positions = $request->input('positions');
        $this->product->sortPhoto($positions);
        return restSuccess('Success');
    }
}