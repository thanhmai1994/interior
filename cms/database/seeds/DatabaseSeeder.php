<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        $this->call(PermissionSeeder::class);
        $this->call(RoleSeeder::class);
        $this->call(ElinkTableSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(LocationDataSeeder::class);
        $this->call(PageSeeder::class);
        $this->call(BlockPageSeeder::class);
        $this->call(MenuSeeder::class);

        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
