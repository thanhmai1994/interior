<?php

use Illuminate\Database\Seeder;
use App\Models\Page;
use App\Models\PageTranslation;


class PageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
    **/

    public function run()
    {
        $pages = [
            [
                'code' => 'HOME',
                'theme' => 'home',
                'active' => 1
            ],
            [
                'code' => 'INTRODUCE',
                'theme' => 'introduce',
                'active' => 1
            ],
            [
                'code' => 'PROCESS',
                'theme' => 'process',
                'active' => 1
            ],
            [
                'code' => 'PROJECT',
                'theme' => 'project',
                'active' => 1
            ],
            [
                'code' => 'DESIGN',
                'theme' => 'design',
                'active' => 1
            ],
            [
                'code' => 'PRODUCT',
                'theme' => 'product',
                'active' => 1
            ],
            [
                'code' => 'CONTACT',
                'theme' => 'contact',
                'active' => 1
            ]
        ];

        foreach($pages as $key => $page){
            if($this->checkExistByCode($page['code'])) {
                continue;
            }
            $page = Page::create($page);    
            $file = resource_path("views/themes/".$page->theme.".blade.php");
            if(!is_file($file)) {
                file_put_contents($file, '');
            } 
        }

        foreach(Page::all() as $page){
            if(!empty($page->title)) {
                continue;
            }
            $input['locale'] = App::getLocale();
            $input['page_id'] = $page->id;
            $input['title'] = $page->code;
            $input['slug'] = strtolower($page->code);
            $page_translation = PageTranslation::create($input);
        }
    }

    private function checkExistByCode($code){
        return (Page::where('code', $code)->first()) ? true : false;
    }
}
