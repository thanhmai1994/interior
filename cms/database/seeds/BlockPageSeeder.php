<?php

use Illuminate\Database\Seeder;
use App\Models\PageBlock;
use App\Models\Page;

class BlockPageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    // const TYPE_URL = 'TYPE_URL';
    // const TYPE_NAME = 'TYPE_NAME';
    // const TYPE_DESCRIPTION = 'TYPE_DESCRIPTION';
    // const TYPE_IMAGE_MAP = 'TYPE_IMAGE_MAP';
    // const TYPE_CONTENT = 'TYPE_CONTENT';
    // const TYPE_ICON = 'TYPE_ICON';
    // const TYPE_PHOTO = 'TYPE_PHOTO';
    // const TYPE_PHOTO_TRANSLATION = 'TYPE_PHOTO_TRANSLATION';
    // const TYPE_MULTI_PHOTOS = 'TYPE_MULTI_PHOTOS';

    public function run()
    {
        $pageBlocks = [
            // Home
            [
                "page_code" => "HOME",
                "parent_code" => "",
                "code" => "HOME-SLIDER",
                "types" => ["TYPE_MULTI_PHOTOS"]
            ],
            [
                "page_code" => "HOME",
                "parent_code" => "",
                "code" => "HOME-GENERAL",
                "types" => ["TYPE_NAME", "TYPE_DESCRIPTION", "TYPE_URL", "TYPE_PHOTO"]
            ],
            [
                "page_code" => "HOME",
                "parent_code" => "",
                "code" => "HOME-IMAGE-CENTER",
                "types" => ["TYPE_PHOTO"]
            ],
            [
                "page_code" => "HOME",
                "parent_code" => "",
                "code" => "HOME-MENU",
                "types" => ["TYPE_NAME"]
            ],
            [
                "page_code" => "HOME",
                "parent_code" => "HOME-MENU",
                "code" => "HOME-MENU-1",
                "types" => ["TYPE_NAME", "TYPE_DESCRIPTION", "TYPE_URL", "TYPE_PHOTO"]
            ],
            [
                "page_code" => "HOME",
                "parent_code" => "HOME-MENU",
                "code" => "HOME-MENU-2",
                "types" => ["TYPE_NAME", "TYPE_DESCRIPTION", "TYPE_URL", "TYPE_PHOTO"]
            ],
            [
                "page_code" => "HOME",
                "parent_code" => "HOME-MENU",
                "code" => "HOME-MENU-3",
                "types" => ["TYPE_NAME", "TYPE_DESCRIPTION", "TYPE_URL", "TYPE_PHOTO"]
            ],
            [
                "page_code" => "HOME",
                "parent_code" => "HOME-MENU",
                "code" => "HOME-MENU-4",
                "types" => ["TYPE_NAME", "TYPE_DESCRIPTION", "TYPE_URL", "TYPE_PHOTO"]
            ],
            [
                "page_code" => "HOME",
                "parent_code" => "HOME-MENU",
                "code" => "HOME-MENU-5",
                "types" => ["TYPE_NAME", "TYPE_DESCRIPTION", "TYPE_URL", "TYPE_PHOTO"]
            ],
            [
                "page_code" => "HOME",
                "parent_code" => "HOME-MENU",
                "code" => "HOME-MENU-6",
                "types" => ["TYPE_NAME", "TYPE_DESCRIPTION", "TYPE_URL", "TYPE_PHOTO"]
            ],
            // End Home


            // Contact
            [
                "page_code" => "CONTACT",
                "parent_code" => "",
                "code" => "CONTACT-SLIDER",
                "types" => ["TYPE_NAME", "TYPE_PHOTO"]
            ],
            [
                "page_code" => "CONTACT",
                "parent_code" => "",
                "code" => "CONTACT-MAP",
                "types" => ["TYPE_DESCRIPTION"]
            ],
            [
                "page_code" => "CONTACT",
                "parent_code" => "",
                "code" => "CONTACT-GENERAL",
                "types" => ["TYPE_NAME", "TYPE_PHOTO", "TYPE_CONTENT", "TYPE_URL"]
            ],
            [
                "page_code" => "CONTACT",
                "parent_code" => "",
                "code" => "CONTACT-FORM",
                "types" => ["TYPE_DESCRIPTION"]
            ],
            // End Contact


            // Introduce
            [
                "page_code" => "INTRODUCE",
                "parent_code" => "",
                "code" => "INTRODUCE-SLIDER",
                "types" => ["TYPE_NAME", "TYPE_PHOTO"]
            ],
            [
                "page_code" => "INTRODUCE",
                "parent_code" => "",
                "code" => "INTRODUCE-GENERAL",
                "types" => ["TYPE_NAME", "TYPE_CONTENT", "TYPE_PHOTO"]
            ],
            // End Introduce


            // Process
            [
                "page_code" => "PROCESS",
                "parent_code" => "",
                "code" => "PROCESS-SLIDER",
                "types" => ["TYPE_NAME", "TYPE_PHOTO"]
            ],
            [
                "page_code" => "PROCESS",
                "parent_code" => "",
                "code" => "PROCESS-GENERAL",
                "types" => ["TYPE_NAME"]
            ],
            [
                "page_code" => "PROCESS",
                "parent_code" => "PROCESS-GENERAL",
                "code" => "PROCESS-GENERAL-1",
                "types" => ["TYPE_NAME", "TYPE_CONTENT", "TYPE_ICON"]
            ],
            [
                "page_code" => "PROCESS",
                "parent_code" => "PROCESS-GENERAL",
                "code" => "PROCESS-GENERAL-2",
                "types" => ["TYPE_NAME", "TYPE_CONTENT", "TYPE_ICON"]
            ],
            [
                "page_code" => "PROCESS",
                "parent_code" => "PROCESS-GENERAL",
                "code" => "PROCESS-GENERAL-3",
                "types" => ["TYPE_NAME", "TYPE_CONTENT", "TYPE_ICON"]
            ],
            [
                "page_code" => "PROCESS",
                "parent_code" => "PROCESS-GENERAL",
                "code" => "PROCESS-GENERAL-4",
                "types" => ["TYPE_NAME", "TYPE_CONTENT", "TYPE_ICON"]
            ],
            [
                "page_code" => "PROCESS",
                "parent_code" => "PROCESS-GENERAL",
                "code" => "PROCESS-GENERAL-5",
                "types" => ["TYPE_NAME", "TYPE_CONTENT", "TYPE_ICON"]
            ],
            [
                "page_code" => "PROCESS",
                "parent_code" => "PROCESS-GENERAL",
                "code" => "PROCESS-GENERAL-6",
                "types" => ["TYPE_NAME", "TYPE_CONTENT", "TYPE_ICON"]
            ],
            // End Process



            // Project
            [
                "page_code" => "PROJECT",
                "parent_code" => "",
                "code" => "PROJECT-SLIDER",
                "types" => ["TYPE_NAME", "TYPE_PHOTO"]
            ],
            [
                "page_code" => "PROJECT",
                "parent_code" => "",
                "code" => "PROJECT-GENERAL",
                "types" => ["TYPE_NAME", "TYPE_CONTENT"]
            ],
            // End Project

            

            // Design
            [
                "page_code" => "DESIGN",
                "parent_code" => "",
                "code" => "DESIGN-SLIDER",
                "types" => ["TYPE_NAME", "TYPE_PHOTO"]
            ],
            [
                "page_code" => "DESIGN",
                "parent_code" => "",
                "code" => "DESIGN-GENERAL",
                "types" => ["TYPE_NAME", "TYPE_CONTENT"]
            ],
            // End Design



            // Product
            [
                "page_code" => "PRODUCT",
                "parent_code" => "",
                "code" => "PRODUCT-SLIDER",
                "types" => ["TYPE_NAME", "TYPE_PHOTO"]
            ],
            [
                "page_code" => "PRODUCT",
                "parent_code" => "",
                "code" => "PRODUCT-GENERAL",
                "types" => ["TYPE_NAME", "TYPE_CONTENT"]
            ],

        ];

        $position = 0;
        foreach($pageBlocks as $key => $pageBlock){
    
            if($this->checkExistByCode($pageBlock['code']))
                continue;

            if($pageBlocks[$position]['page_code'] != $pageBlock['page_code'])
                $position = 0;

            $pageBlock['position'] = $position;
            $pageBlock['page_id'] = $this->getIdPageByCode($pageBlock['page_code']);
            if($pageBlock['parent_code'])
                $pageBlock['parent_id'] = $this->getIdPageBlockByCode($pageBlock['parent_code']);
            $pageBlock['types'] = json_encode($pageBlock['types']);
            unset($pageBlock['page_code']);
            unset($pageBlock['parent_code']);
            PageBlock::create($pageBlock);

            $position++;
        }
    }

    private function getIdPageByCode($code){
        return Page::where('code', $code)->first()->id;
    }

    private function getIdPageBlockByCode($code){
        return PageBlock::where('code', $code)->first()->id;
    }

    private function checkExistByCode($code){
        return (PageBlock::where('code', $code)->first()) ? true : false;
    }
}
