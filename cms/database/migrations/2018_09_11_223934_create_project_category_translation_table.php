<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectCategoryTranslationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_category_translation', function (Blueprint $table) {
            $table->increments('id');
            $table->string('locale')->index();
            $table->integer('project_category_id')->unsigned();
            
            $table->string('slug')->nullable();
            $table->string('name')->nullable();
            
            $table->unique(['project_category_id','locale']);
            $table->foreign('project_category_id')->references('id')->on('project_category')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_category_translation');
    }
}
