<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCareerTranslationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('career_translation', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('career_id')->unsigned();
            $table->string('locale')->index();
            $table->string('name')->nullable();
            $table->string('salary')->nullable();
            $table->string('working_form')->nullable();
            $table->string('slug')->nullable();
            $table->longText('description')->nullable();
            $table->longText('request')->nullable();
            $table->longText('benefit')->nullable();

            $table->unique(['career_id','locale']);
            $table->foreign('career_id')->references('id')->on('careers')->onDelete('cascade');  
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('career_translation');
    }
}
