$(document).ready(function () {
    var job = $('#fijob');
    var doc = $('#fidoc');
    var value_job = '';
    var value_combo = '';
    var coefficient = 0;
    var interest_rate = 0;

    var group = job.parents('.col-sm-10').next('.col-sm-10');

    $('#fijob').change(function () {
        value_job = $(this).val();

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: URL_JOB,
            type: 'POST',
            data: {
                value_job: value_job,
                value_combo: value_combo
            }
        }).done(function (res) {
            if(!res.result && value_job && value_combo) {
                group.addClass('disabled');
                $('#warningChooseModal').modal('show');
            } else if (res.result) {
                $('#much-you-need').attr("data-max", parseFloat(res.result.max_money));
                $('#much-you-need').attr("data-min", parseFloat(res.result.min_money));
                $('#much-you-need').attr("data-step", parseFloat(res.result.step_money));

                $('#payment-term').attr("data-max", parseFloat(res.result.max_borrow_time));
                $('#payment-term').attr("data-min", parseFloat(res.result.min_borrow_time));

                $('.much-you-need-span').text(parseFloat(res.result.min_money).toString().replace(/\B(?=(\d{3})+(?!\d))/g, "."));
                $('.payment-term-span').text(parseFloat(res.result.min_borrow_time));

                coefficient = parseFloat(res.result.coefficient);
                interest_rate = parseFloat(res.result.interest_rate);

                range();
            }
        }).fail(function (res) {
            //
        });
    });

    // Get document from job choose
    $('#fijob').change(function () {
        let job_val_get_combo = $(this).val();

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: URL_GET_COMBO,
            type: 'POST',
            data: {
                job_val_get_combo : job_val_get_combo
            }
        }).done(function (res) {
            $('.docsTable').html();
            $('.docsTable').html(res);
        }).fail(function (res) {
            //
        });
    });

    $(document).on('click', '.choose_doc', function () {
        value_combo = $(this).data('val');

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: URL_JOB,
            type: 'POST',
            data: {
                value_job: value_job,
                value_combo: value_combo
            }
        }).done(function (res) {
            if (!res.result && value_job && value_combo) {
                group.addClass('disabled');
                $('#warningChooseModal').modal('show');
            } else if (res.result) {
                
                $('#much-you-need').attr("data-max", parseFloat(res.result.max_money));
                $('#much-you-need').attr("data-min", parseFloat(res.result.min_money));
                $('#much-you-need').attr("data-step", parseFloat(res.result.step_money));

                $('#payment-term').attr("data-max", parseFloat(res.result.max_borrow_time));
                $('#payment-term').attr("data-min", parseFloat(res.result.min_borrow_time));

                $('.much-you-need-span').text(parseFloat(res.result.min_money).toString().replace(/\B(?=(\d{3})+(?!\d))/g, "."));
                $('.payment-term-span').text(parseFloat(res.result.min_borrow_time));

                coefficient = parseFloat(res.result.coefficient);
                interest_rate = parseFloat(res.result.interest_rate);

                range();
            }
        }).fail(function (res) {
            //
        });
    });


    function range() {
     
        var $range = $('.form-range');
        if ($range.length) {
            function _muchYouNeed() {
                var $muchYouNeed = $('#much-you-need');
                var dataPrefix = $muchYouNeed.data('prefix'),
                    dataMin = parseFloat($muchYouNeed.attr('data-min')),
                    dataMax = parseFloat($muchYouNeed.attr("data-max")),
                    dataFrom = parseFloat($muchYouNeed.attr('data-min')),
                    dataStep = parseFloat($muchYouNeed.attr("data-step"));
                if (typeof dataPrefix === 'undefined') {
                    dataPrefix = ''
                }
                if (typeof dataMin === 'undefined') {
                    dataMin = 0
                }
                if (typeof dataMax === 'undefined') {
                    dataMax = 10
                }
                if (typeof dataFrom === 'undefined') {
                    dataFrom = 0
                }
                if (typeof dataStep === 'undefined') {
                    dataStep = 1
                }
                _ionRangeSlider($muchYouNeed, dataMin, dataMax, dataFrom, dataStep, dataPrefix)
            }

            function _paymentTerms() {
                var $paymentTerms = $('#payment-term');
                var dataPrefix = $paymentTerms.data('prefix'),
                    dataMin = parseFloat($paymentTerms.attr('data-min')),
                    dataMax = parseFloat($paymentTerms.attr("data-max")),
                    dataFrom = parseFloat($paymentTerms.attr('data-min')),
                    dataStep = parseFloat($paymentTerms.attr("data-step"));
                if (typeof dataPrefix === 'undefined') {
                    dataPrefix = ''
                }
                if (typeof dataMin === 'undefined') {
                    dataMin = 3
                }
                if (typeof dataMax === 'undefined') {
                    dataMax = 24
                }
                if (typeof dataFrom === 'undefined') {
                    dataFrom = 3
                }
                if (typeof dataStep === 'undefined') {
                    dataStep = 1
                }
                _ionRangeSlider($paymentTerms, dataMin, dataMax, dataFrom, dataStep, dataPrefix)
            }

            function _ionRangeSlider($el, dataMin, dataMax, dataFrom, dataStep, dataPrefix) {
                var slider = $el.data("ionRangeSlider");

                slider.update({
                    min: dataMin,
                    max: dataMax,
                    from: dataFrom,
                    step: dataStep,
                });
            }

            function prettify(num) {
                var n = num.toString();
                return n.replace(/(\d{1,3}(?=(?:\d\d\d)+(?!\d)))/g, '$1' + '.')
            }

            function _checkRange() {
                var month = $('#payment-term').val();
                var money = $('#much-you-need').val(); // Gọi hàm tính lãi
            }

            function _calc(money, month) {
                var result = $('.fiSolution__divider b');
                var interest = 0.01; // Danh sách lãi xuất theo tháng: ở đây cũng là ví dụ
                switch (parseInt(month)) {
                    case 6:
                    case 7:
                    case 8:
                    case 9:
                    case 10:
                        interest = 0.01;
                        break;
                    case 11:
                    case 12:
                    case 13:
                    case 14:
                    case 15:
                        interest = 0.0115;
                        break;
                    case 16:
                    case 17:
                    case 18:
                    case 19:
                    case 20:
                        interest = 0.025;
                        break;
                    case 21:
                    case 22:
                    case 23:
                    case 24:
                    case 25:
                        interest = 0.035;
                        break;
                    case 26:
                    case 27:
                    case 28:
                    case 29:
                    case 30:
                        interest = 0.05;
                        break;
                    default:
                        interest = 0.07;
                } // Công thức tính khoản vay. tạm thời ví dụ là vậy
                var recipe = interest * money; // Xử lý để render ra HTML
                var temp = Number(recipe).toFixed(0);
                result.html(prettify(temp) + '<sup>\u0111</b>')
            }
            _muchYouNeed();
            _paymentTerms()
        }
    }

    $('#much-you-need').change(function(){
        var money = parseFloat($(this).val());
        var duration = parseFloat($('#payment-term').val());
        var result = calculatorLoan(money, interest_rate, duration, coefficient);
        $('.result-loan').text(result);
    });

    $('#payment-term').change(function () {
        var duration = parseFloat($(this).val());
        var money = parseFloat($('#much-you-need').val());
        var result = calculatorLoan(money, interest_rate, duration, coefficient);
        $('.result-loan').text(result);
    });

    /* 
        Công thức tính khoản tiền trả hàng tháng
        c: Tổng số tiền vay
        r: Lãi suất hàng tháng
        duration: Số tháng vay
        x: Hệ số công thức
    */
    function calculatorLoan(c, r, duration, x) {
        var mauso = 1 - 1 / (Math.pow((1 + (r / 12) * 0.01), duration));
        return Math.ceil(x * c * (((r / 12) * 0.01) / mauso)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
    }
    
    $('.btn-loan').click(function(e){
        e.preventDefault();
        var job_id = $('#fijob').val(), // Công việc hiện tại
            combo_id = $('#fidoc').val(), // Giấy tờ cung cấp
            money = $('#much-you-need').val(), // Khoản vay
            month = $('#payment-term').val(), // Thời hạn vay
            name = $('#finame').val(), // Tên
            phone = $('#fiphone').val(), // Số điện thoại
            email = $('#fiemail').val(), // Email
            city = $('#ficity').val(), // Thành phố
            pay = $('.result-loan').text();  // Khoản trả hàng tháng

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: URL_LOAN,
            type: 'POST',
            data: {
                job_id: job_id,
                combo_id: combo_id,
                money: money,
                month: month,
                name: name,
                phone: phone,
                email: email,
                city: city,
                pay: pay,
                coefficient: coefficient,
                interest_rate: interest_rate
            }
        }).done(function (res) {
            if (res.status_code === 200) {
                $('#successModal').modal('show');
            }
        }).fail(function (res) {
            $('.error-sign-up').empty();
            if (res.status === 422) {

                var errorsHtml = '<div style="padding-left: 15px;">';

                $.each(res.responseJSON.message, function (key, value) {
                    errorsHtml += '<h3 style="list-style-type: none;text-align: left;">' + value[0] + '</h3>';
                });
                errorsHtml += '</div>';

                $('.error-sign-up').html(errorsHtml);
            } else {
                // do some thing else
            }

            $('#errorsModal').modal('show');
        });
    });
});